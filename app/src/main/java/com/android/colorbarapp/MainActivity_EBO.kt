package com.android.colorbarapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.android.colorbarapp.Fragment.EBO.*
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main_ebo.*

class MainActivity_EBO : AppCompatActivity() , BottomNavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_ebo)

        bottom_navigation.setOnNavigationItemSelectedListener(this)
        supportFragmentManager.beginTransaction().replace(R.id.framlay,
            Consolidate_Fragment()
        ).commit()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.nav_consolidate->{
                supportFragmentManager.beginTransaction().replace(R.id.framlay,
                    Consolidate_Fragment()
                ).commit()
            }
      R.id.nav_product->{
                supportFragmentManager.beginTransaction().replace(R.id.framlay,
                    Product_Report_Fragment()
                ).commit()
            }

   R.id.nav_store->{
                supportFragmentManager.beginTransaction().replace(R.id.framlay,
                    Store_Fragment()
                ).commit()
            }

   R.id.nav_monthlyorders->{
                supportFragmentManager.beginTransaction().replace(R.id.framlay,
                    Monthly_Order_Fragment()
                ).commit()
            }

   R.id.nav_dailyorders->{
                supportFragmentManager.beginTransaction().replace(R.id.framlay,
                    Daily_Orders_Fragment()
                ).commit()
            }


        }

        return true
    }
}
