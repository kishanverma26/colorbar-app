package com.s.strokeclock.Utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.location.LocationManager
import android.media.AudioManager
import android.net.Uri
import android.provider.MediaStore
import android.text.TextUtils
import android.text.format.DateFormat
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.PopupMenu
import androidx.core.app.ActivityCompat
import com.android.colorbarapp.Login_Activity
import com.android.colorbarapp.MainActivity_EBO
import com.android.colorbarapp.MainActivity_Managment
import com.android.colorbarapp.R
import com.android.colorbarapp.Utils.UserSharedPrefrences
import com.android.volley.error.*
import com.google.android.material.snackbar.Snackbar
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList

class Common {


    companion object
    {

        fun getFormatedNumber(number: String): String? {
            return if (!number.isEmpty()) {
                val `val` = number.toDouble()
                NumberFormat.getNumberInstance(Locale("en", "in")).format(`val`)
            } else {
                "0"
            }
        }

        fun get_valuinlaks(number: String): String? {
            return if (!number.isEmpty()) {
                val value1 = number.toDouble()
//                loggg("value",value1.toString()+"")
                if (value1<10){
                    "0.0"
                }else {
                    (value1 / 100000).toString()
                }
            } else {
                "0.0"
            }
        }

        fun formateNulltoValue(number: String): String? {
            if (number.equals("null")){
                return "0.0"
            }
            return number
        }

        fun TxtFormate(stringval: String) : String{
            val str : String
            var longformatelist = ArrayList<String>()
            longformatelist.add("EXCLUSIVE STORES")
            longformatelist.add("CORPORATE OFFICE HO.")
            longformatelist.add("E-Commerce")
         longformatelist.add("Accessories")
      longformatelist.add("MESMEREYES KAJAL")
       longformatelist.add("NON TRADED GOODS")
       longformatelist.add("EXCLUSIVE STORES")
   longformatelist.add("EXCLUSIVE STORES(COEARTH)")
 longformatelist.add("EXCLUSIVE STORES(XOXO)")

            var sortformatelist = ArrayList<String>()
            sortformatelist.add("EBO")
            sortformatelist.add("CORP. OFFice.")
            sortformatelist.add("E-Com")
              sortformatelist.add("Acce.")
             sortformatelist.add("MESM. KAJAL")
           sortformatelist.add("Non Traded gds")
          sortformatelist.add("EXCL. STORES.")
  sortformatelist.add("EBO(COEARTH)")
 sortformatelist.add("EBO(XOXO)")

            if (longformatelist.contains(stringval)){
                str =sortformatelist.get(longformatelist.indexOf(stringval))
            }else{
                str = stringval
            }
            return str
        }

        private fun getcurrentdate(): String? {
            val c = Calendar.getInstance()
//            println("Current time => " + c.time)
            val df =
                SimpleDateFormat("dd/MM/yyyy", Locale.US)
            return df.format(c.time)
        }

        fun seven_day(string: String): String?{
            val df =
                SimpleDateFormat("dd/MM/yyyy", Locale.US)
            val c = Calendar.getInstance()
            val myDate: Date = df.parse(string)
            c.time = myDate
            c.add(Calendar.DAY_OF_YEAR, -7);
            val newDate = c.getTime()
            val date = df.format(newDate);
            loggg("7date",date)
            return  date
        }

        fun one_month(string: String):String?{
            val df =
                SimpleDateFormat("dd/MM/yyyy", Locale.US)
            val c = Calendar.getInstance()
            val myDate: Date = df.parse(string)
            c.time = myDate

            c.add(Calendar.MONTH, -1);
            val newDate = c.getTime()

            val date = df.format(newDate);
            loggg("1date",date)
            return  getdate_start_end()
        }

        fun roundof( valuestr: String): Double {
            val str = valuestr
            var index = str.lastIndexOf('.')+2
            if (str.substring( str.indexOf("."), str.length).length>3) {
                index =  index+1
            }
//            println(str.substring(0, index))
//          println(str.substring(0, index).replace("E",""))
            var finalconver="0.0"
            if (str.substring(0, index).contains("E")) {
                finalconver=  str.substring(0, index).replace("E", "")
            }else{
                finalconver=   str.substring(0, index)
            }
//            println("1m"+finalconver)

//            println("- "+)


//            loggg("tradesprecentage1",str+" - "+str.substring( str.indexOf("."), str.length))
            return finalconver.toDouble()
        }

        fun roundofstring( valuestr: String): String {
            val str = valuestr
            if (str.contains(".")) {
                val index = str.lastIndexOf('.')
//                println(str.substring(0, index))
//            loggg("valuee",str.substring(0, index))
                return str.substring(0, index)
            }else{
                return str
            }
        }


        fun dateof( valuestr: String): String {
            val str = valuestr
            val index = str.indexOf('/')
//            println(str.substring(0, index))
//            loggg("valuee",str.substring(0, index))
            return str.substring(0, index)
        }
        fun dateafterof( valuestr: String): String {
            val str = valuestr
            val index = str.indexOf('/')
//            loggg("valuee",str.substring(index, valuestr.length))
            return str.substring(index, valuestr.length)
        }
 fun datelastof( valuestr: String): String {
            val str = valuestr
            val index = str.lastIndexOf('/')
            return str.substring(0,index)
        }

        fun hasText(context: Context, editText: EditText, info: String): Boolean {
            val text = editText.text.toString().trim { it <= ' ' }
            if (editText.text.trim().isEmpty()) {
                val snack: Snackbar = Snackbar.make(
                        editText,
                        info + " " + context.getString(R.string.common_error),
                        Snackbar.LENGTH_LONG
                )
                snack.show()
                return false
            }
            return true
        }

        fun isValidEmail(email: EditText): Boolean {

            var b:  Boolean


            if (Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email.text.toString()).matches()){
                b =true
            }else{
                b = false
                Snakbar("Please enter a valid email",email)
            }
            return b
        }
        fun checkGPSStatus(mContext: Context): Boolean {
            var locationManager: LocationManager? = null
            var gps_enabled = false

            if (locationManager == null) {
                locationManager =
                    mContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            }

            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
                gps_enabled = true
            else
                gps_enabled = false
            return gps_enabled
        }



        // onactivity result
        //  requestCode=: 101 ,permissions=android.permission.WRITE_EXTERNAL_STORAGE ,grantResults=-1  deny
        //requestCode=: 101 ,permissions=android.permission.WRITE_EXTERNAL_STORAGE ,grantResults=0 allow
        fun askpermission(activity: AppCompatActivity, PERMISSIONS: Array<String>) {
            val PERMISSION_ALL = 1
            if (!hasPermissions(activity, *PERMISSIONS)) {
                ActivityCompat.requestPermissions(activity, PERMISSIONS, PERMISSION_ALL)
            }
        }

        fun hasPermissions(context: Context?, vararg permissions: String): Boolean {
            if (context != null && permissions != null) {
                for (permission in permissions) {
                    if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                        return false
                    }
                }
            }
            return true
        }


        public fun isEmailValid(email: String): Boolean {
                return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
            }


        fun isValidMobile(phone: String): Boolean {
            return android.util.Patterns.PHONE.matcher(phone).matches()
        }


  fun isValidpass(pass: String): Boolean {
      var status = false
           if (pass.length>5)
           {
               status =true
           }

      return  status
        }

        fun toast(context: Context,str:String){
            Toast.makeText(context,str,Toast.LENGTH_LONG).show()
        }

        fun isJSONValid(test: String): Boolean {
            try {
                JSONObject(test)
            } catch (ex: JSONException) {
            }

            return true
        }

        @Throws(JSONException::class)
        fun ParseString(obj: JSONObject, Param: String): String {
            if (obj.has(Param)) {
                val lastSeen = obj.getString(Param)
                return if (lastSeen != null && !TextUtils.isEmpty(lastSeen) && !lastSeen.equals(
                        "null",
                        ignoreCase = true
                    ) && lastSeen.length > 0
                )
                    obj.getString(Param)
                else
                    ""

            } else
                return ""
        }


        fun Parsintentvalue(key: String, inte: AppCompatActivity): String? {
            val data: String?

            if (inte.intent.hasExtra(key)) {
                data = inte.intent.getStringExtra(key)
            } else {
                data = ""
            }

            return data

        }


        fun Snakbar(msg: String, v: View) {
            Snackbar.make(v, msg, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

        fun checklength(text: EditText): Int {

            return text.text.toString().trim { it <= ' ' }.length
        }


        fun timestamptodate(inputDate: String): Date? {
            Log.e("timestamp", getDate(java.lang.Long.parseLong(inputDate))!!.toString() + "")
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US)
            var date: Date? = null
            try {
                date = simpleDateFormat.parse(inputDate)
            } catch (e: ParseException) {
                e.printStackTrace()
                Log.e("dateexception", "", e)
                //throw new IllegalAccessException("Error in parsing date");
            }

            return date
        }

        fun datetotimestamp(datestr: String): String {
            val simpleDateFormat = SimpleDateFormat("dd:MM:yyyy HH:mm:ss", Locale.US)
            val date = simpleDateFormat.parse(datestr);
            return date.time.toString()
        }


        fun getdate(time: Long): String {
            val cal = Calendar.getInstance(Locale.ENGLISH)
            cal.timeInMillis = time
            val date = DateFormat.format("dd:MM:yyyy HH:mm", cal).toString()
            return date
        }

        fun getDate(time: Long): Date? {
            loggg("date3", time.toString() + "")
            val cal = Calendar.getInstance(Locale.ENGLISH)
            cal.timeInMillis = time
            val date = DateFormat.format("yyyy-MM-dd hh:mm:ss", cal).toString()
            Log.e("date1", date)
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US)
            var datee: Date? = null
            try {
                datee = simpleDateFormat.parse(date)
                Log.e("date2", datee!!.toString() + "")
            } catch (e: ParseException) {
                e.printStackTrace()
                Log.e("dateexception", "", e)
                //throw new IllegalAccessException("Error in parsing date");
            }

            return datee
        }

       fun getTime(time: Long): String? {
            loggg("date3", time.toString() + "")
            val cal = Calendar.getInstance(Locale.ENGLISH)
            cal.timeInMillis = time
            val date = DateFormat.format("hh:mm:ss", cal).toString()
            Log.e("date1", date)


            return date
        }



          fun getTimeDiffrace(time: Long,datestr:String): String? {
            val cal = Calendar.getInstance(Locale.ENGLISH)
            cal.timeInMillis = time
            val date = DateFormat.format("dd:MM:yyyy HH:mm", cal).toString()

              val     simpleDateFormat1 =  SimpleDateFormat("dd:MM:yyyy HH:mm")
              val  date1 = simpleDateFormat1.parse(date)

              val     simpleDateFormat =  SimpleDateFormat("dd:MM:yyyy HH:mm")
              val  date2 = simpleDateFormat.parse(datestr)

              val difference = date1.getTime() -date2.getTime()
                Log.e("currentdate",date+" -------- "+datestr+" ---- "+date1+" ----- "+date2);

              val days  =  (difference / (1000*60*60*24));
              val hours = ((difference - (1000*60*60*24*days)) / (1000*60*60));
              val min =  (difference - (1000*60*60*24*days) - (1000*60*60*hours)) / (1000*60)
              return DecimalFormat("00").format(hours)+":"+DecimalFormat("00").format(min)
        }

          fun getTimeDiffrace(datestr:String): String? {


//
//              val     simpleDateFormat =  SimpleDateFormat("dd:MM:yyyy HH:mm")
//              val  date2 = simpleDateFormat.parse(datestr)
//                Log.e("hour",date2.getTime().toString()+" -----" )
//              val difference = date2.getTime()
//
//              val days  =  (difference / (1000*60*60*24));
//              val hours = ((difference - (1000*60*60*24*days)) / (1000*60*60));
//              val min =  (difference - (1000*60*60*24*days) - (1000*60*60*hours)) / (1000*60)
              return datestr.substring(datestr.indexOf(" ")+1)
        }

        private fun getTimeDistanceInMinutes(time: Long): Int {
            val timeDistance = currentDate().time - time
            return Math.round((Math.abs(timeDistance) / 1000 / 60).toFloat())
        }

        fun currentDate(): Date {
            val calendar = Calendar.getInstance()
            return calendar.time
        }


        fun hideSoftKeyboard(activity: AppCompatActivity) {
            val inputMethodManager = activity.getSystemService(
                AppCompatActivity.INPUT_METHOD_SERVICE
            ) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(
                activity.window.decorView.rootView.windowToken, 0
            )

        }

        fun popup_Menu(context: Context,activity: Activity,imageView: ImageView) {
            val p = PopupMenu(context, imageView)
            p.getMenuInflater().inflate(R.menu.popup_menu, p.getMenu())
            p.menu.getItem(1).setVisible(false)
            p.menu.getItem(2).setVisible(false)

            if (UserSharedPrefrences.getInstance(context).getjsonrole().length>0){
                var jary = JSONArray(UserSharedPrefrences.getInstance(context).getjsonrole())
                for (i in 0..jary.length() - 1) {
                    if (jary.getString(i).equals("cluster_team")){
                        p.menu.getItem(2).setVisible(true)
                    }
                    if (jary.getString(i).equals("zonal_team")){
                        p.menu.getItem(1).setVisible(true)
                    }

                }
            }

            if (UserSharedPrefrences.getInstance(context).getrole().equals("zonal_team")){
                p.menu.getItem(1).setVisible(false)
            }
            if (UserSharedPrefrences.getInstance(context).getrole().equals("cluster_team")){
                p.menu.getItem(2).setVisible(false)
            }


            p.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override
                fun onMenuItemClick(item: MenuItem): Boolean {
                    when(item.itemId){
                        R.id.tv_logout->{
                            UserSharedPrefrences.getInstance(context).Clear()
                            activity.startActivity(Intent(context, Login_Activity::class.java))
                            activity!!.finish()

                            p.dismiss()
                        }

                        R.id.tv_switchebo->{
                            UserSharedPrefrences.getInstance(context).setrole("zonal_team")
                            activity.startActivity(Intent(context, MainActivity_EBO::class.java))
                            activity!!.finish()
                            p.dismiss()
                        }
                        R.id.tv_switchmanagment->{
                            UserSharedPrefrences.getInstance(context).setrole("cluster_team")
                            activity.startActivity(Intent(context, MainActivity_Managment::class.java))
                            activity!!.finish()
                            p.dismiss()
                        }


                    }
                    Toast.makeText(context, item.getTitle(), Toast.LENGTH_SHORT).show()
                    return true
                }
            })
            p.show()
        }

        fun hideSoftKeyboardinfragment(activity: Activity ) {
            val inputMethodManager = activity.getSystemService(
                Context.INPUT_METHOD_SERVICE
            ) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(
                activity.window.decorView.rootView.windowToken, 0
            )

        }

        fun time_diff(posttime: Long): String {
            var time = ""
            val numberOfDays: Int
            val numberOfHours: Int
            val numberOfMinutes: Int
                  val numberOfsec: Int

            try {

                var difference = System.currentTimeMillis() - posttime
                difference = difference / 1000

                numberOfDays = (difference / 86400).toInt()
                numberOfHours = (difference % 86400 / 3600).toInt()
                numberOfMinutes = (difference % 86400 % 3600 / 60).toInt()
                numberOfsec = (difference % 86400 % 3600 % 60).toInt()
                val date = Date(posttime)
                val sdf = SimpleDateFormat("MMM dd", Locale.US)
                val sdf1 = SimpleDateFormat("HH:mm", Locale.US)
                sdf.timeZone = TimeZone.getDefault()
                sdf1.timeZone = TimeZone.getDefault()
                val java_date = sdf.format(date) + " at " + sdf1.format(date)
                Log.e("time_diff", "$numberOfDays,$numberOfHours,$numberOfMinutes,$numberOfsec")

                if (numberOfDays > 0) {

                    if (numberOfDays > 0) {

                        time = sdf.format(date) + " at " + sdf1.format(date)
                    }
                } else if (numberOfHours > 0) {
                    // if (numberOfHours == 1) {
                    time = "$numberOfHours hr"
                    //                } else {
                    //                    time = numberOfHours + " hours ago";
                    //                }

                } else if (numberOfMinutes > 0) {


                    //                if (numberOfMinutes == 1) {

                    time = "$numberOfMinutes min"
                    //                } else {
                    //                    time = numberOfMinutes + " minutes ago";
                    //                }
                } else {
                    time = "Just now"
                }

            } catch (e: Exception) {
                e.printStackTrace()
                Log.e("excptn", "excptn", e)
            }

            return time
        }

        fun isCallActive(context: Context): Boolean {
            val manager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager

            return manager.mode == AudioManager.MODE_IN_CALL
        }

        fun set_Image_bytearray(ImageUri: Uri, context: Context): ByteArray {
            var b = ByteArray(0)
            try {
                val bitmap = MediaStore.Images.Media.getBitmap(context.contentResolver, ImageUri)

                val baos = ByteArrayOutputStream()
                val nh = (bitmap.height * (512.0 / bitmap.width)).toInt()
                val scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true)
                scaled.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                b = baos.toByteArray()
                Log.e("byteArray", b.toString() + "")

            } catch (e: Exception) {
                Log.e("e", "e", e)
                e.printStackTrace()
            }

            return b
        }

        fun Image_Loading_Url(url: String?): String {
            var URL = "abc"
            if (url != null) {


                if (url.length > 0) {
                    if (url.contains("https://")) {
                        URL = url
                    } else if (url.contains("emulated/0")) {
                        URL = url
                    } else if (url.contains("file:///data/user/0/")) {
                        URL = url
                    } else if (url.contains("/storage/")) {
                        URL = url
                    } else {
                                        URL = "http://68.183.88.83/Grabbt/public/images/" + url;
                    }
                }
            }
            return URL
        }

        fun volleyerror(volleyError: VolleyError): String {
            var message: String? = "Somthing went wrong please try again."
            if (volleyError is NetworkError) {
                message = "Cannot connect to Internet...Please check your connection!"
            } else if (volleyError is ServerError) {
                message = "The server could not be found. Please try again after some time!!"
            } else if (volleyError is AuthFailureError) {
                message = "Cannot connect to Internet...Please check your connection!"
            } else if (volleyError is ParseError) {
                message = "Parsing error! Please try again after some time!!"
            } else if (volleyError is NoConnectionError) {
                message = "Cannot connect to Internet...Please check your connection!"
            } else if (volleyError is TimeoutError) {
                message = "Connection TimeOut! Please check your internet connection."
            }
            return message!!
        }

               fun loggg(tag: String, msg: String) {
            Log.e(tag, msg)

        }
         fun loggg(tag: String, msg: String,th :Throwable) {
            Log.e(tag, msg,th)

        }


         fun current_date(): String? {
            val c = Calendar.getInstance()
            val df =
                    SimpleDateFormat("dd/MM/yyyy", Locale.US)
            return df.format(c.time)
        }


        fun current_date_formated(position: Int): String? {
            var df =SimpleDateFormat("dd MMM", Locale.US)
            val c = Calendar.getInstance()
//            println("Current time => " + c.time)
            if (position==0) {
                df =
                        SimpleDateFormat("dd MMM", Locale.US)
            }else if (position==1){
                df =
                        SimpleDateFormat("dd/MM", Locale.US)
            }else if (position==2){
                df =
                        SimpleDateFormat("MMM", Locale.US)
            }
            return df.format(c.time)
        }

        fun getdate_start_end() : String{
            val calendar = Calendar.getInstance()
            calendar.add(Calendar.MONTH, 0)
            calendar[Calendar.DATE] = calendar.getActualMinimum(Calendar.DAY_OF_MONTH)
            val monthFirstDay = calendar.time
            calendar[Calendar.DATE] = calendar.getActualMaximum(Calendar.DAY_OF_MONTH)
            val monthLastDay = calendar.time

            val df = SimpleDateFormat("dd/MM/yyyy")
            val startDateStr = df.format(monthFirstDay)


            Log.e("DateFirstLast", "$startDateStr")
            return  "$startDateStr"
        }

    }



}