package com.android.colorbarapp.Utils
import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.util.Log

class UserSharedPrefrences {


    lateinit var context: Context

    //    var preferences: UserSharedPrefrences? = null
    lateinit  var userPreferences: SharedPreferences
    lateinit var playeruserPreferences:SharedPreferences
    lateinit   var edit: SharedPreferences.Editor
    lateinit   var playeredit:SharedPreferences.Editor

    companion object {
        var preferences: UserSharedPrefrences? = null
        fun getInstance(context: Context): UserSharedPrefrences {


            if (preferences == null) {
                preferences = UserSharedPrefrences(context.applicationContext)
            }
            return preferences as UserSharedPrefrences
        }


        var preferences1: UserSharedPrefrences? = null
        fun getInstancee(context: Context): UserSharedPrefrences {


            if (preferences1 == null) {
                preferences1 = UserSharedPrefrences(context.applicationContext,"")
            }
            return preferences1 as UserSharedPrefrences
        }
    }


    constructor(context: Context){
        userPreferences =
            context.getSharedPreferences("UserSharedPreferences", Context.MODE_PRIVATE)
        edit = userPreferences.edit()
    }


    constructor(context: Context,s:String){
        playeruserPreferences =
            context.getSharedPreferences("UserSharedPreferencesplayer", Context.MODE_PRIVATE)
        playeredit = playeruserPreferences.edit()
    }

    fun Clear() {
        edit.clear()
        edit.commit()
    }


    fun setname(name: String) {

        edit.putString("name", name)
        edit.commit()
    }

    fun getname(): String {

        return userPreferences.getString("name", "")!!
    }

    fun setlname(name: String) {

        edit.putString("lname", name)
        edit.commit()
    }

    fun getlname(): String {

        return userPreferences.getString("lname", "")!!
    }


    fun setemail(email: String) {
        Log.e("email", email)
        edit.putString("email", email)
        edit.commit()
    }

    fun getemail(): String {
        return userPreferences.getString("email", "")!!
    }

    fun setlogin(login: Boolean?) {
        edit.putBoolean("login", login!!)
        edit.commit()
    }

    fun getlogin(): Boolean? {
        return userPreferences.getBoolean("login", false)
    }


    fun gettoken(): String? {
        return userPreferences.getString("token", "")!!
    }


    fun settoken(token: String) {
        edit.putString("token", token)
        edit.commit()
    }

    fun getrole(): String {
        return userPreferences.getString("role", "")!!
    }


    fun setrole(role: String) {
        edit.putString("role", role)
        edit.commit()
    }
    fun getjsonrole(): String {
        return userPreferences.getString("jsonrole", "")!!
    }


    fun setjsonrole(role: String) {
        edit.putString("jsonrole", role)
        edit.commit()
    }

}