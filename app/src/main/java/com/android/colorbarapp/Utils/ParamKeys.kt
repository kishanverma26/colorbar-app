package com.android.colorbarapp.Utils

interface ParamKeys {



  //    Live server

  val baseurl: String get() = "http://colorbarappdev.lyf.fyi/api/"
//  val baseurl: String get() = "https://app.colorbarcosmetics.com/api/"
  val baseurl_login: String   get() = "${baseurl}cognito/"
  val baseurl_managment: String   get() = "${baseurl}management/"
  val baseurl_ebo: String   get() = "${baseurl}ebo/"

  val Id : String   get() = "id"
  val F_name : String   get() = "f_name"
  val L_name : String   get() = "l_name"
  val Gender : String   get() = "gender"
  val Email : String   get() = "email"
  val Phone : String   get() = "phone"

  //    Paper
  val TradeBook : String   get() = "TradeBook"
  val TradeJson : String   get() = "TradeJson"

  val TradeNameBook : String   get() = "TradeNameBook"
  val TradeNameres : String   get() = "TradeJson"

  val CategoryBook : String   get() = "CategoryBook"
  val CategoryJson : String   get() = "CategoryJson"
  val CategoryTableBook : String   get() = "CategoryTableBook"
  val CategoryTableJson : String   get() = "CategoryJson"

  val RegionBook : String   get() = "RegionBook"
  val RegionJson : String   get() = "RegionJson"


  val ConsolidatedBook : String   get() = "ConsolidatedBook"
  val ConsolidatedJson : String   get() = "ConsolidatedJson"

  val EBOproductBook : String   get() = "EBOproductBook"
  val EboKeyresonse : String   get() = "Keyresonse"

  val Storeresonse : String   get() = "Storeresonse"
  val Dailyorderres : String   get() = "Dailyorderres"

 val MonthlyBook : String   get() = "MonthlyBook"
  val Monthlyres : String   get() = "Monthlyres"


}
