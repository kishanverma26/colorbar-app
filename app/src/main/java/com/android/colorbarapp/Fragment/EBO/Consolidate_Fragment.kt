package com.android.colorbarapp.Fragment.EBO

import android.content.Context
import android.graphics.Color
import android.graphics.LinearGradient
import android.graphics.Shader
import android.os.Bundle
import android.text.TextPaint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.android.colorbarapp.Adapter.EBO.Product_data_Adapter
import com.android.colorbarapp.Adapter.EBO.Revenue_Adapter
import com.android.colorbarapp.CustomClasses.DelayedProgressDialog
import com.android.colorbarapp.CustomClasses.MySingleton
import com.android.colorbarapp.CustomClasses.SpacesItemDecoration
import com.android.colorbarapp.Model.Monthly_Order
import com.android.colorbarapp.R
import com.android.colorbarapp.Utils.ParamKeys
import com.android.colorbarapp.Utils.UserSharedPrefrences
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.request.StringRequest
import com.s.strokeclock.Utils.Common
import io.paperdb.Paper
import kotlinx.android.synthetic.main.consolidate_fragmnet.*
import kotlinx.android.synthetic.main.consolidate_fragmnet.view.*
import kotlinx.android.synthetic.main.consolidate_fragmnet.view.tvproducts
import kotlinx.android.synthetic.main.header_topbar.view.*
import org.json.JSONObject

class Consolidate_Fragment : Fragment() , ParamKeys{

    lateinit var mcoxt: Context
    lateinit var view1: View
    lateinit var listproducts: ArrayList<Monthly_Order>
    lateinit var listbills: ArrayList<Monthly_Order>
    lateinit var listrevenue: ArrayList<Monthly_Order>
    lateinit var listcustomers: ArrayList<Monthly_Order>


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.consolidate_fragmnet,container,false)
        mcoxt= activity!!
        view1 = view
        initi();
        return view
    }

    private fun initi() {


        val paint: TextPaint = view1.tvproducts.getPaint()
        val width: Float = paint.measureText("Products")

        val textShader: Shader = LinearGradient(
            0F, 0F, width, view1.tvproducts.getTextSize(), intArrayOf(
                Color.parseColor("#5b3e3e"),
                Color.parseColor("#ac8686"),
                Color.parseColor("#5b3e3e")
            ), null, Shader.TileMode.CLAMP
        )
        view1.tvproducts.getPaint().setShader(textShader)

        val paint2: TextPaint = view1.tvbills.getPaint()
        val width2: Float = paint2.measureText("Bills")

        val textShader2: Shader = LinearGradient(
            0F, 0F, width2, view1.tvbills.getTextSize(), intArrayOf(
                Color.parseColor("#5b3e3e"),
                Color.parseColor("#ac8686"),
                Color.parseColor("#5b3e3e")
            ), null, Shader.TileMode.CLAMP
        )
        view1.tvbills.getPaint().setShader(textShader2)

        val paint3: TextPaint = view1.tvrevenue.getPaint()
        val width3: Float = paint3.measureText("Revenue")

        val textShader3: Shader = LinearGradient(
            0F, 0F, width3, view1.tvrevenue.getTextSize(), intArrayOf(
                Color.parseColor("#5b3e3e"),
                Color.parseColor("#ac8686"),
                Color.parseColor("#5b3e3e")
            ), null, Shader.TileMode.CLAMP
        )
        view1.tvrevenue.getPaint().setShader(textShader3)

        val paint4: TextPaint = view1.tvcustomer.getPaint()
        val width4: Float = paint4.measureText("Customer")

        val textShader4: Shader = LinearGradient(
            0F, 0F, width4, view1.tvcustomer.getTextSize(), intArrayOf(
                Color.parseColor("#5b3e3e"),
                Color.parseColor("#ac8686"),
                Color.parseColor("#5b3e3e")
            ), null, Shader.TileMode.CLAMP
        )
        view1.tvcustomer.getPaint().setShader(textShader4)

        val spacingInPixels =resources.getDimensionPixelSize(R.dimen.d_20dp)
        view1.recycler.layoutManager = GridLayoutManager(activity,2)
        view1.recycler.addItemDecoration(SpacesItemDecoration(2, spacingInPixels, true))




        view1.recycler_bills.layoutManager = GridLayoutManager(activity,2)
        view1.recycler_bills.addItemDecoration(SpacesItemDecoration(2, resources.getDimensionPixelSize(R.dimen.d_20dp), true))



      view1.recycler_rev.layoutManager = GridLayoutManager(activity,2)
        view1.recycler_rev.addItemDecoration(SpacesItemDecoration(2, resources.getDimensionPixelSize(R.dimen.d_20dp), true))



    view1.recycler_cust.layoutManager = GridLayoutManager(activity,2)
        view1.recycler_cust.addItemDecoration(SpacesItemDecoration(2, resources.getDimensionPixelSize(R.dimen.d_20dp), true))


        view1.tvusername.setText(UserSharedPrefrences.getInstance(activity!!).getname())
        view1. iv_icon.setOnClickListener { v -> Common.popup_Menu(activity!!,activity!!,view1. iv_icon) }


        if (Paper.book(ConsolidatedBook).read<String>(ConsolidatedJson)!=null){
            setdataonui(Paper.book(ConsolidatedBook).read<String>(ConsolidatedJson))
        }


        getdata(activity!!)
    }

       private fun getdata(context: Context) {


        val pd= DelayedProgressDialog()
        pd.show(fragmentManager!!,"")
        val url = baseurl_ebo+"getconsolidated"

        val stringRequest: StringRequest = object: StringRequest(Method.GET,url,
                Response.Listener {
                    Common.loggg("gethome", it)
                    Common.loggg("url", url)
                    Paper.book(ConsolidatedBook).write(ConsolidatedJson,it)

                    setdataonui(it)
                    pd.cancel()

                },
                Response.ErrorListener {
                    pd.cancel()
                    Common.toast(context, Common.volleyerror(it))
                })
        {
            override  fun getParams(): Map<String, String>  {
                val params2: MutableMap<String, String> = HashMap()

//                params2["name"]= "${personName}"
                return params2
            }

            override fun getHeaders(): MutableMap<String, String> {
                val params2: MutableMap<String, String> = HashMap()
                params2.put("Authorization","Bearer ${UserSharedPrefrences.getInstance(context).gettoken()}")
                return params2
            }

        }
        stringRequest.setShouldCache(false)
        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy
        MySingleton.getInstance(context).addToRequestQueue(stringRequest)
    }

    private fun setdataonui(it: String?) {
        listproducts = ArrayList()
        listbills = ArrayList()
        listcustomers = ArrayList()
        listrevenue = ArrayList()
        try {


            var obj1= JSONObject(it)
            if(obj1.getString("status").equals("true")) {
                var obj =obj1.getJSONObject("data")
//                Product data
                val jsonObjectproducts = obj.getJSONObject("products")

                listproducts.add(Monthly_Order("Today",jsonObjectproducts.getString("today")))
                listproducts.add(Monthly_Order("Yesterday",jsonObjectproducts.getString("yesterday")))
                listproducts.add(Monthly_Order("This Month",jsonObjectproducts.getString("thisMonth")))
                listproducts.add(Monthly_Order("Last Month",jsonObjectproducts.getString("lastMonth")))

                view1.recycler.adapter = Product_data_Adapter(activity, listproducts)

//                bills data
                val jsonObjectbills = obj.getJSONObject("bills")

                listbills.add(Monthly_Order("Today",jsonObjectbills.getString("today")))
                listbills.add(Monthly_Order("Yesterday",jsonObjectbills.getString("yesterday")))
                listbills.add(Monthly_Order("This Month",jsonObjectbills.getString("thisMonth")))
                listbills.add(Monthly_Order("Last Month",jsonObjectbills.getString("lastMonth")))

                view1.recycler_bills.adapter = Product_data_Adapter(activity, listbills)


//                revenue data
                val jsonObjectrevenue = obj.getJSONObject("revenue")

                listrevenue.add(Monthly_Order("Today",jsonObjectrevenue.getString("today")))
                listrevenue.add(Monthly_Order("Yesterday",jsonObjectrevenue.getString("yesterday")))
                listrevenue.add(Monthly_Order("This Month",jsonObjectrevenue.getString("thisMonth")))
                listrevenue.add(Monthly_Order("Last Month",jsonObjectrevenue.getString("lastMonth")))

                view1.recycler_rev.adapter = Revenue_Adapter(activity, listrevenue)


//                revenue data
                val jsonObjectcustomers = obj.getJSONObject("customers")

                listcustomers.add(Monthly_Order("Today",jsonObjectcustomers.getString("today")))
                listcustomers.add(Monthly_Order("Yesterday",jsonObjectcustomers.getString("yesterday")))
                listcustomers.add(Monthly_Order("This Month",jsonObjectcustomers.getString("thisMonth")))
                listcustomers.add(Monthly_Order("Last Month",jsonObjectcustomers.getString("lastMonth")))

                view1.recycler_cust.adapter = Product_data_Adapter(activity, listcustomers)



            }else{

            }
        }catch (e: Exception) {
            Log.e("error","",e);
        }
    }


}