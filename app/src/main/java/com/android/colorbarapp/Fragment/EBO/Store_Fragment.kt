package com.android.colorbarapp.Fragment.EBO

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.transition.Slide
import androidx.transition.Transition
import androidx.transition.TransitionManager
import com.android.colorbarapp.Adapter.Store_Adapter
import com.android.colorbarapp.CustomClasses.DelayedProgressDialog
import com.android.colorbarapp.CustomClasses.MySingleton
import com.android.colorbarapp.Login_Activity
import com.android.colorbarapp.Model.Product
import com.android.colorbarapp.Model.Store
import com.android.colorbarapp.R
import com.android.colorbarapp.Utils.ParamKeys
import com.android.colorbarapp.Utils.UserSharedPrefrences
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.request.StringRequest
import com.archit.calendardaterangepicker.customviews.CalendarListener
import com.archit.calendardaterangepicker.customviews.DateRangeCalendarViewApi
import com.s.strokeclock.Utils.Common
import com.s.strokeclock.Utils.Common.Companion.getFormatedNumber
import io.paperdb.Paper
import kotlinx.android.synthetic.main.calender_view.view.*
import kotlinx.android.synthetic.main.header_topbar.*
import kotlinx.android.synthetic.main.header_topbar.view.*
import kotlinx.android.synthetic.main.store_report_fragmnet.view.*
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class Store_Fragment : Fragment() , ParamKeys ,View.OnClickListener{

    lateinit var mcoxt: Context
    lateinit var view1: View
    lateinit var list: ArrayList<Store>
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.store_report_fragmnet,container,false)
        mcoxt= activity!!
        view1 = view
        initi();
        return view
    }

    private fun initi() {
        list = ArrayList()
        setupcalenderview()
        view1.ivcalender.setOnClickListener(this)
        view1.ivcal.setOnClickListener(this)
        view1.recycler.layoutManager = LinearLayoutManager(activity)
        view1.recycler.adapter = Store_Adapter(activity,list)
        view1.tvusername.setText(UserSharedPrefrences.getInstance(activity!!).getname())
        view1. iv_icon.setOnClickListener { v -> Common.popup_Menu(activity!!,activity!!,view1. iv_icon) }


        if (Paper.book(EBOproductBook).read<String>(Storeresonse)!=null){
            setup_tabledata_ui(Paper.book(EBOproductBook).read<String>(Storeresonse))
        }
        view1. ivcalender.text = "${Common.datelastof(Common.current_date()!!)}"
        getdata(activity!!,Common.current_date()!!,0)

    }


    private fun getdata(context: Context,datestr :String,postype:Int) {


        val pd= DelayedProgressDialog()
        pd.show(fragmentManager!!,"")
        val url = baseurl_ebo+"getstorereport?date=$datestr"

        val stringRequest: StringRequest = object: StringRequest(Method.GET,url,
                Response.Listener {
                    Common.loggg("getstorereport", it)
                    Common.loggg("url", url)
                    if (postype==0) {
                        Paper.book(EBOproductBook).write(Storeresonse, it)
                    }
                    setup_tabledata_ui(it)
                    pd.cancel()

                },
                Response.ErrorListener {
                    pd.cancel()
                    Common.toast(context, Common.volleyerror(it))
                })
        {
            override  fun getParams(): Map<String, String>  {
                val params2: MutableMap<String, String> = HashMap()

//                params2["name"]= "${personName}"
                return params2
            }
            override fun getHeaders(): MutableMap<String, String> {
                val params2: MutableMap<String, String> = HashMap()
                params2.put("Authorization","Bearer ${UserSharedPrefrences.getInstance(context).gettoken()}")
               Common. loggg("header",params2.toString())
                return params2
            }
        }
        stringRequest.setShouldCache(false)
        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy
        MySingleton.getInstance(context).addToRequestQueue(stringRequest)
    }

    var total_sales = 0.0
    var total_sales_daily = 0.0

    private fun setup_tabledata_ui(it: String?) {
        list.clear()
        total_sales = 0.0
        total_sales_daily = 0.0
        try {


            var obj= JSONObject(it)
            if(obj.getString("status").equals("true")) {
                val jsonObject = obj.getJSONArray("data")

                if (jsonObject.length() > 0){


//                category

                    for (i in 0..jsonObject.length() - 1) {
                        val jsonObjecttrade = jsonObject.getJSONObject(i)

                        list.add(
                                Store(
                                        Common.TxtFormate(jsonObjecttrade.getString("storeName")),
                                        Common.TxtFormate( jsonObjecttrade.getString("daily")),
                                        Common.roundofstring(jsonObjecttrade.getString("monthly"))))

                        total_sales =  total_sales + jsonObjecttrade.getString("monthly").toDouble()
                        total_sales_daily =  total_sales_daily + jsonObjecttrade.getString("daily").toDouble()

                    }

                    view1.recycler.adapter!!.notifyDataSetChanged()

                    view1. tv_monthly_total.text =getFormatedNumber(total_sales.toString())
                    view1. tv_daily_total.text = getFormatedNumber(total_sales_daily.toString())

//                    view1.scrollView.visibility = View.VISIBLE
//                    view1.rl_empty.visibility = View.GONE

                }else{
//
//                    view1.scrollView.visibility = View.GONE
//                    view1.rl_empty.visibility = View.VISIBLE

                }

            }else{
//                view1.scrollView.visibility = View.GONE
//                view1.rl_empty.visibility = View.VISIBLE

            }

        }catch (e: Exception) {

            Log.e("error","",e);
        }
    }

    override fun onClick(v: View?) {
        when(v!!.id) {
            R.id.ivcalender -> {
                shocalender()
                strcalenderdate1 = ""


            }
            R.id.ivcal -> {
                shocalender()
                strcalenderdate1 = ""


            }
            R.id.tvclose -> {

                hidecalender()
            }

        }
    }

    var strcalenderdate1=""

    private fun setupcalenderview() {
        view1.  tvclose.setOnClickListener(this)
        view1. tvok.visibility = View.GONE

        view1. clcalendar.setCalendarListener(object : CalendarListener {

            override fun onDateRangeSelected(startDate: Calendar, endDate: Calendar) {
                val df =
                        SimpleDateFormat("dd/MM/yyyy", Locale.US)
                Toast.makeText(
                        activity,
                        "Start Date: " +  df.format(startDate.time)
                                .toString() + " End date: " +df.format( endDate.time).toString(),
                        Toast.LENGTH_SHORT
                ).show()

                strcalenderdate1 =df.format(startDate.time)


            }

            override fun onFirstDateSelected(startDate: Calendar) {
                val df =
                        SimpleDateFormat("dd/MM/yyyy", Locale.US)
                strcalenderdate1 =df.format(startDate.time)


                hidecalender()
//                showcalenderdate(View.VISIBLE)
                    getdata( activity!!,strcalenderdate1,1)
//
                view1. ivcalender.text = "${Common.datelastof(strcalenderdate1)}"

            }
        })


        val startMonth = Calendar.getInstance()
        startMonth.add(Calendar.YEAR, -20)
        view1. clcalendar.setVisibleMonthRange(startMonth, Calendar.getInstance())
        view1. clcalendar.setSelectableDateRange(startMonth, Calendar.getInstance())
        view1. clcalendar.setCurrentMonth(Calendar.getInstance())
        view1. clcalendar.setCurrentMonth(Calendar.getInstance())

//        view1. clcalendar.setSelectedDateRange(startSelectedDate,  Calendar.getInstance());
    }

    fun shocalender(){
        view1. clcalendar.resetAllSelectedViews()
        val transition: Transition = Slide(Gravity.RIGHT)
        transition.setDuration(600)
        transition.addTarget(view1.view_calender)
        TransitionManager.beginDelayedTransition(view1.rlparant, transition);
        view1.view_calender.visibility = View.VISIBLE

    }
    fun hidecalender(){
        val transition: Transition = Slide(Gravity.RIGHT)
        transition.setDuration(600)
        transition.addTarget(view1.view_calender)
        TransitionManager.beginDelayedTransition(view1.rlparant, transition);
        view1. view_calender.visibility = View.GONE


    }


}