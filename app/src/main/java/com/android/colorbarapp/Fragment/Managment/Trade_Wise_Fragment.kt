package com.android.colorbarapp.Fragment.Managment

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.LinearGradient
import android.graphics.Shader
import android.net.Uri
import android.os.Bundle
import android.text.TextPaint
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.transition.Slide
import androidx.transition.Transition
import androidx.transition.TransitionManager
import com.android.colorbarapp.Adapter.Managment.Expendable_Trade_Wise_Adapter
import com.android.colorbarapp.Adapter.Managment.Gross_Wise_Adapter
import com.android.colorbarapp.BuildConfig
import com.android.colorbarapp.CustomClasses.DelayedProgressDialog
import com.android.colorbarapp.CustomClasses.MySingleton
import com.android.colorbarapp.Model.ExpendableTrade
import com.android.colorbarapp.Model.Gross
import com.android.colorbarapp.Model.Trade
import com.android.colorbarapp.R
import com.android.colorbarapp.Trade_wise_Detail_Activity
import com.android.colorbarapp.Utils.ParamKeys
import com.android.colorbarapp.Utils.UserSharedPrefrences
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.request.StringRequest
import com.archit.calendardaterangepicker.customviews.CalendarListener
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.utils.MPPointF
import com.opencsv.CSVWriter
import com.s.strokeclock.Utils.Common
import com.s.strokeclock.Utils.Common.Companion.TxtFormate
import com.s.strokeclock.Utils.Common.Companion.current_date_formated
import com.s.strokeclock.Utils.Common.Companion.datelastof
import com.s.strokeclock.Utils.Common.Companion.formateNulltoValue
import com.s.strokeclock.Utils.Common.Companion.getFormatedNumber
import com.s.strokeclock.Utils.Common.Companion.get_valuinlaks
import com.s.strokeclock.Utils.Common.Companion.loggg
import com.s.strokeclock.Utils.Common.Companion.one_month
import com.s.strokeclock.Utils.Common.Companion.roundof
import com.s.strokeclock.Utils.Common.Companion.toast
import kotlinx.android.synthetic.main.calender_view.view.*
import kotlinx.android.synthetic.main.header_topbar.view.*
import kotlinx.android.synthetic.main.trade_wise_fragment.view.*
import org.json.JSONObject
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class Trade_Wise_Fragment : Fragment() ,View.OnClickListener , ParamKeys {

    lateinit var mcoxt: Context
    lateinit var view1: View
    lateinit var list: ArrayList<ExpendableTrade>
    lateinit var list_new: ArrayList<ExpendableTrade>
    lateinit var sbCaList: ArrayList<Trade>
    lateinit var sbCaList_of_list: ArrayList<ArrayList<Trade>>
    lateinit var list_2: ArrayList<Gross>
    lateinit var list_newgross: ArrayList<Gross>

    var listofstr= arrayOf<String>("GT",
        "GT(XOXO)",
        "GT(COEARTH)",
        "MT",
        "MT(XOXO)",
        "MT(COEARTH)",
        "EBO",
        "EBO(XOXO)",
        "EBO(COEARTH)",
        "FRANCHISE",
        "FRANCHISE(XOXO)",
        "FRANCHISE(COEARTH)",
        "ACADEMY SALE",
        "E-COM",
        "E-COM(XOXO)",
        "E-COM(COEARTH)",
        "E-COM MP",
        "E-COM MP(XOXO)",
        "E-COM MP(COEARTH)",
        "INSTITUTION",
        "MODICARE",
        "WEBSITE",
        "WEBSITE(XOXO)",
        "WEBSITE(COEARTH)",
        "CB PRO")
    var entries1 = ArrayList<PieEntry>()
    var strcaldate = ""
    var calenderpos = 0   // 0 = M , 1=Week , 2 = Day

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.trade_wise_fragment,container,false)
        mcoxt= activity!!
        view1 = view
        initi();
        return view
    }

    private fun initi() {
        strcaldate = Common.current_date()!!
        list = ArrayList()
        sbCaList = ArrayList()
        sbCaList_of_list = ArrayList()
        list_new = ArrayList()
        list_2 = ArrayList()
        list_newgross = ArrayList()
//     list_2_xoxo = ArrayList()

        val paint: TextPaint = view1.tvmanagemnt.getPaint()
        val width: Float = paint.measureText("Management Report Summary")
        val width1: Float = paint.measureText("Gross Sale & Sale Return Data")

        val textShader: Shader = LinearGradient(
            0F, 0F, width, view1.tvmanagemnt.getTextSize(), intArrayOf(
                Color.parseColor("#5b3e3e"),
                Color.parseColor("#ac8686"),
                Color.parseColor("#5b3e3e")
            ), null, Shader.TileMode.CLAMP
        )
        val textShader1: Shader = LinearGradient(
            0F, 0F, width1, view1.tv_gross.getTextSize(), intArrayOf(
                Color.parseColor("#5b3e3e"),
                Color.parseColor("#ac8686"),
                Color.parseColor("#5b3e3e")
            ), null, Shader.TileMode.CLAMP
        )

        view1.tvmanagemnt.getPaint().setShader(textShader)
        view1.tv_gross.getPaint().setShader(textShader)

        view1.ivcalender.setOnClickListener(this)
        view1.tvshare.setOnClickListener(this)
        view1.tv_day.setOnClickListener(this)
        view1.tv_week.setOnClickListener(this)
        view1.tv_month.setOnClickListener(this)
        view1.recycler.layoutManager = LinearLayoutManager(activity)
        view1.recycler_table.layoutManager = LinearLayoutManager(activity)


        view1.iv_detail.setOnClickListener(this)
        view1.tvusername.setText(UserSharedPrefrences.getInstance(activity!!).getname())
        view1. iv_icon.setOnClickListener { v -> Common.popup_Menu(activity!!,activity!!,view1. iv_icon) }


        setupcalenderview()

        view1.tv_monthdate.text = current_date_formated(2)
//        if (Paper.book(TradeBook).read<String>(TradeJson)!=null){
////            loggg("paperr","inn")
////            setdataonui(Paper.book(TradeBook).read<String>(TradeJson))
//        }else {
//            loggg("paperr","api")
            getdata(one_month(strcaldate).toString(),strcaldate,activity!!)
//        }

    }


    var strcalenderdate1=""
    var strcalenderdate2=""

    private fun setupcalenderview() {
        view1.  tvclose.setOnClickListener(this)
        view1. tvok.setOnClickListener(this)

        view1. clcalendar.setCalendarListener(object : CalendarListener {

            override fun onDateRangeSelected(startDate: Calendar, endDate: Calendar) {
                val df =
                    SimpleDateFormat("dd/MM/yyyy", Locale.US)
                Toast.makeText(
                    activity,
                    "Start Date: " +  df.format(startDate.time)
                        .toString() + " End date: " +df.format( endDate.time).toString(),
                    Toast.LENGTH_SHORT
                ).show()

                strcalenderdate1 =df.format(startDate.time)
                strcalenderdate2 =df.format(endDate.time)

            }

            override fun onFirstDateSelected(startDate: Calendar) {
                val df =
                    SimpleDateFormat("dd/MM/yyyy", Locale.US)
                strcalenderdate1 =df.format(startDate.time)
            }
        })


        val startMonth = Calendar.getInstance()
        startMonth.add(Calendar.YEAR, -20)
        view1. clcalendar.setVisibleMonthRange(startMonth, Calendar.getInstance())
        view1. clcalendar.setSelectableDateRange(startMonth, Calendar.getInstance())
        view1. clcalendar.setCurrentMonth(Calendar.getInstance())
//        view1. clcalendar.setSelectedDateRange(startSelectedDate,  Calendar.getInstance());
    }

    fun addchartdata(chartnew : PieChart,entries : ArrayList<PieEntry>){
        chartnew.setUsePercentValues(true)
        chartnew.setHoleRadius(50f)
        chartnew.setTransparentCircleColor(R.color.colorAccent)
        chartnew.setTransparentCircleAlpha(10)
//        chartnew.setExtraOffsets(-100f,100f,0f,0f)
        chartnew.setHoleColor(12176592)
        chartnew.getDescription().setEnabled(false)
        chartnew.legend.isEnabled = false
        chartnew.legend.isWordWrapEnabled = true
        chartnew.setEntryLabelColor(R.color.bottom_icon_color)
        chartnew.setEntryLabelTextSize(12f)
        chartnew.setDrawEntryLabels(true)

        chartnew.setExtraTopOffset(15f);
        chartnew.setExtraBottomOffset(15f)
        chartnew.setExtraLeftOffset(10f)
        chartnew.setExtraRightOffset(10f)


        var dataSet = PieDataSet(entries, "")

        dataSet.setDrawIcons(false)
        dataSet.setDrawValues(true)



        dataSet.setSliceSpace(2f)
        dataSet.setIconsOffset(MPPointF(0F, 40F))
        dataSet.setSelectionShift(10f)
        val colorFirst = let { ContextCompat.getColor(activity!!, R.color.grapc1) }
        val colorFirst2 = let { ContextCompat.getColor(activity!!, R.color.grapc2) }
        val colorFirst3 = let { ContextCompat.getColor(activity!!, R.color.grapc3) }
        val colorFirst4 = let { ContextCompat.getColor(activity!!, R.color.grapc4) }
        val colorFirst5 = let { ContextCompat.getColor(activity!!, R.color.grapc5) }
        val colorFirst6 = let { ContextCompat.getColor(activity!!, R.color.grapc6) }

        val legends=  chartnew.legend
        legends.verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
        legends.horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT
        legends.orientation = Legend.LegendOrientation.VERTICAL


        val colorFirstlist  = mutableListOf(colorFirst,colorFirst2,colorFirst3,colorFirst4,colorFirst5,colorFirst6)

        dataSet.colors =colorFirstlist

        dataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        dataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
//        dataSet.setValueLinePart1OffsetPercentage(10f);
        dataSet.setValueLinePart1Length(.43f);
        dataSet.setValueLinePart2Length(.4f);

        //dataSet.setSelectionShift(0f);
        val data = PieData(dataSet)
        data.setValueFormatter(PercentFormatter(chartnew))

        data.setValueTextSize(12f)
        data.setValueTextColor(R.color.bottomsheet_blue)

        chartnew.setData(data)
        chartnew.invalidate()
    }
    fun showcalenderdate(vale:Int){
        if (vale==View.VISIBLE) {
            view1.lldate.visibility = View.GONE
            view1.tv_calenderdate.visibility = View.VISIBLE
        }else{
            view1.lldate.visibility = View.VISIBLE
            view1.tv_calenderdate.visibility = View.GONE
        }
    }
    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.tvshare->{
                sendcsv()
            }
            R.id.iv_detail->{
                startActivity(Intent(activity!!,Trade_wise_Detail_Activity::class.java))
            }
            R.id.ivcalender->{
                shocalender()
                strcalenderdate1 =""
                strcalenderdate2 =""

            }
            R.id.tvclose->{

                hidecalender()
            }
            R.id.tvok->{
                view1. clcalendar.resetAllSelectedViews()
                if (strcalenderdate1.length>0) {
                    hidecalender()
                    showcalenderdate(View.VISIBLE)
                    if (strcalenderdate2.length>0){

                        getdata(strcalenderdate1,strcalenderdate2,activity!!)

                        view1.tv_calenderdate.text = "${datelastof(strcalenderdate1)} - ${datelastof(strcalenderdate2)}"
                    }else{
                        getdata(strcalenderdate1,strcalenderdate1,activity!!)

                        view1.tv_calenderdate.text = "${datelastof(strcalenderdate1)}"
                    }
                    view1.tv_day.setTextColor(ContextCompat.getColor(activity!!,R.color.bottomsheet_blue))
                    view1.tv_day.background = null
                    view1.tv_week.setTextColor(ContextCompat.getColor(activity!!,R.color.bottomsheet_blue))
                    view1.tv_week.background = null
                    view1.tv_month.setTextColor(ContextCompat.getColor(activity!!,R.color.bottomsheet_blue))
                    view1.tv_month.background = null

                    view1.tv_daydate.visibility = View.GONE
                    view1.tv_weekdate.visibility = View.VISIBLE
                    view1.tv_monthdate.visibility = View.GONE

                }else{
                    toast(activity!!,"Please select date")
                }
            }

            R.id.tv_day->{
                calenderpos =2
//                getdata("04/03/2020","04/03/2020",activity!!)


                getdata(Common.current_date().toString(),Common.current_date().toString(),activity!!)
                changecolor(view1.tv_day,view1.tv_week,view1.tv_month)
                view1.tv_weekdate.visibility = View.GONE
                view1.tv_monthdate.visibility = View.GONE
                view1.tv_daydate.visibility = View.VISIBLE
                view1.tv_daydate.text = current_date_formated(0)
//                view1.tv_daydate.text ="08 Mar"
                showcalenderdate(View.GONE)
            }
            R.id.tv_week->{
                calenderpos =1
//                getdata("01/03/2020","07/03/2020",activity!!)
                getdata(seven_day(strcaldate).toString(),strcaldate,activity!!)

                changecolor(view1.tv_week,view1.tv_day,view1.tv_month)

                view1.tv_monthdate.visibility = View.GONE
                view1.tv_daydate.visibility = View.GONE
                view1.tv_weekdate.visibility = View.VISIBLE
//              seven_day("07/03/2020")
//                view1.tv_weekdate.text = "1/03 - 7/03"
                showcalenderdate(View.GONE)
            }
            R.id.tv_month->{
                calenderpos =0
//                getdata("01/03/2020","31/03/2020",activity!!)
                getdata(one_month(strcaldate).toString(),strcaldate,activity!!)

                changecolor(view1.tv_month,view1.tv_day,view1.tv_week)

                view1.tv_daydate.visibility = View.GONE
                view1.tv_weekdate.visibility = View.GONE
                view1.tv_monthdate.visibility = View.VISIBLE
                view1.tv_monthdate.text = current_date_formated(2)
//                view1.tv_monthdate.text ="Mar"

                showcalenderdate(View.GONE)
            }

        }
    }





    var total :Long = 0
    private fun getdata(fromdate:String,todate:String,context: Context) {
        total =0
        entries1.clear()
        list.clear()
        list_new.clear()
        list_newgross.clear()
//        addvalues()
        list_2.clear()


        val pd= DelayedProgressDialog()
        pd.isCancelable =false
        pd.show(fragmentManager!!,"")
        val url = baseurl_managment+"gethome?from_date=$fromdate&to_date=$todate"
        val stringRequest: StringRequest = object:StringRequest(Method.GET,url,
            Response.Listener {
                loggg("gethome",it)
                loggg("url",url)
                setdataonui_sortvalues(it,fromdate, todate, context)
                pd.cancel()

            },
            Response.ErrorListener {
                pd.cancel()
                toast(context,Common.volleyerror(it))
            })
        {
            override  fun getParams(): Map<String, String>  {
                val params2: MutableMap<String, String> = HashMap()

//                params2["name"]= "${personName}"
                return params2
            }
            override fun getHeaders(): MutableMap<String, String> {
                val params2: MutableMap<String, String> = HashMap()
                params2.put("Authorization","Bearer ${UserSharedPrefrences.getInstance(context).gettoken()}")
                Log.e("auth",params2.toString())
                return params2
            }
        }
        stringRequest.setShouldCache(false)
        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy
        MySingleton.getInstance(context).addToRequestQueue(stringRequest)
    }


    fun shocalender(){
        val transition: Transition = Slide(Gravity.RIGHT)
        transition.setDuration(600)
        transition.addTarget(view1.view_calender)
        TransitionManager.beginDelayedTransition(view1.rlparant, transition);
        view1.view_calender.visibility = View.VISIBLE

    }
    fun hidecalender(){
        val transition: Transition = Slide(Gravity.RIGHT)
        transition.setDuration(600)
        transition.addTarget(view1.view_calender)
        TransitionManager.beginDelayedTransition(view1.rlparant, transition);
        view1. view_calender.visibility = View.GONE


    }



    fun changecolor(textView: TextView, textView1: TextView, textView2: TextView){
        textView.setTextColor(ContextCompat.getColor(activity!!,R.color.white))
        textView.background = ContextCompat.getDrawable(activity!!,R.drawable.selected_tab_bg)

        textView1.setTextColor(ContextCompat.getColor(activity!!,R.color.bottomsheet_blue))
        textView1.background = null

        textView2.setTextColor(ContextCompat.getColor(activity!!,R.color.bottomsheet_blue))
        textView2.background = null
    }


    private fun date(): String? {
        val c = Calendar.getInstance()
//        println("Current time => " + c.time)
        val df =
            SimpleDateFormat("dd/MM/yyyy", Locale.US)
        return df.format(c.time)
    }


    var total_sales = 0.0
    var target_sales = 0.0
    var mtd_Amount = 0.0
    var lymtd_Amount = 0.0
    var gross_Amount = 0.0
    var return_Amount = 0.0
    var tv_growthtotal = 0.0
//
//    fun setdataonui(it:String,fromdate:String,todate:String,context:Context){
//        total_sales = 0.0
//        target_sales = 0.0
//        mtd_Amount = 0.0
//        lymtd_Amount = 0.0
//        gross_Amount = 0.0
//        return_Amount = 0.0
//        tv_growthtotal = 0.0
//        try {
//            var obj= JSONObject(it)
//            if(obj.getString("status").equals("true")) {
//                val jsonObject = obj.getJSONObject("data")
//                total = jsonObject.getLong("total_Sales")
//                if (total > 0){
//                    view1.tv_totalsales.setText("₹ "+getFormatedNumber(total.toString()))
//
//
////                trade
//                    if (jsonObject.getJSONArray("trades").length()>0){
//                        var tradelong :Double= 0.0
//                        for (i in 0 until jsonObject.getJSONArray("trades").length()) {
//                            val jsonObjecttrade = jsonObject.getJSONArray("trades").getJSONObject(i)
//                            val  roundvalue = roundof((jsonObjecttrade.getLong("total_Sale_Amount").toFloat()/ total * 100).toString())
//                            if (roundvalue>0) {
//                                entries1.add(
//                                    PieEntry(
//                                        roundvalue.toFloat()
//                                        , TxtFormate(jsonObjecttrade.getString("trade_Name"))
//                                    )
//                                )
//                            }
//                            if (!listofstr.contains(TxtFormate(jsonObjecttrade.getString("trade_Name")))) {
//                                var expendableValue = ExpendableTrade(
//                                    TxtFormate(jsonObjecttrade.getString("trade_Name")),
//                                    jsonObjecttrade.getString("current_Month_Target"),
//                                    jsonObjecttrade.getString("mtd_Amount"),
//                                    jsonObjecttrade.getString("lmtd_Amount"),
//                                    jsonObjecttrade.getString("growth_Percentage")
//                                )
//
//                                if (jsonObjecttrade.getJSONArray("subGroup").length()>0){
//                                    for (i in 0 until jsonObjecttrade.getJSONArray("subGroup").length()) {
//                                        var jsonSubCatArray = jsonObjecttrade.getJSONArray("subGroup").getJSONObject(i)
//                                        sbCaList.add(
//                                            Trade(
//                                                TxtFormate(jsonSubCatArray.getString("trade_Name")),
//                                                jsonSubCatArray.getString("current_Month_Target"),
//                                                jsonSubCatArray.getString("mtd_Amount"),
//                                                jsonSubCatArray.getString("lmtd_Amount"),
//                                                jsonSubCatArray.getString("growth_Percentage")
//                                        ))
//                                    }
//                                    expendableValue.tradelist = sbCaList
//                                }
//                                list.add(expendableValue)
//                                loggg("valuesCheck","if")
//                            }else{
//                                loggg("valuesCheck","else")
//                                var expendableValue = ExpendableTrade(
//                                    TxtFormate(jsonObjecttrade.getString("trade_Name")),
//                                    jsonObjecttrade.getString("current_Month_Target"),
//                                    jsonObjecttrade.getString("mtd_Amount"),
//                                    jsonObjecttrade.getString("lmtd_Amount"),
//                                    jsonObjecttrade.getString("growth_Percentage")
//                                )
//                                val positionOfItem =listofstr.indexOf(TxtFormate(jsonObjecttrade.getString("trade_Name")))
//                                loggg("valuesCheck","${listofstr.get(positionOfItem)} - ${TxtFormate(jsonObjecttrade.getString("trade_Name"))} - $positionOfItem")
//                                if (listofstr.get(positionOfItem).equals(TxtFormate(jsonObjecttrade.getString("trade_Name")))){
//                                if (jsonObjecttrade.getJSONArray("subGroup").length()>0){
//                                    for (i in 0 until jsonObjecttrade.getJSONArray("subGroup").length()) {
//                                        var jsonSubCatArray = jsonObjecttrade.getJSONArray("subGroup").getJSONObject(i)
//                                        sbCaList.add(
//                                            Trade(
//                                                TxtFormate(jsonSubCatArray.getString("trade_Name")),
//                                                jsonSubCatArray.getString("current_Month_Target"),
//                                                jsonSubCatArray.getString("mtd_Amount"),
//                                                jsonSubCatArray.getString("lmtd_Amount"),
//                                                jsonSubCatArray.getString("growth_Percentage")
//                                            ))
//                                    }
//                                    expendableValue.tradelist = sbCaList
//                                }}
//                                list_new.set(positionOfItem,
//                                    expendableValue
//                                )
//                                sbCaList.clear()
//                            }
//                            list_2.add(
//                                Gross(
//                                    TxtFormate(jsonObjecttrade.getString("trade_Name")),
//                                    jsonObjecttrade.getString("gross_Amount"),
//                                    jsonObjecttrade.getString("return_Amount"),
//                                    jsonObjecttrade.getString("total_Sale_Amount")
//                                )
//                            )
//
//                            total_sales += jsonObjecttrade.getString("total_Sale_Amount").toDouble()
//
//                            if (!jsonObjecttrade.getString("current_Month_Target").equals("null")){
//                                target_sales += jsonObjecttrade.getString("current_Month_Target")
//                                    .toDouble()
//                            }
//                            mtd_Amount += jsonObjecttrade.getString("mtd_Amount").toDouble()
//                            lymtd_Amount += jsonObjecttrade.getString("lmtd_Amount").toDouble()
//                            gross_Amount += jsonObjecttrade.getString("gross_Amount").toDouble()
//                            return_Amount += jsonObjecttrade.getString("return_Amount").toDouble()
//                            tv_growthtotal += jsonObjecttrade.getString("growth_Percentage")
//                                .toDouble()
//                        }
//
//                        if (tradelong!=0.0){
//                            entries1.add(
//                                PieEntry(
//                                    tradelong.toFloat()
//                                    , "Other"
//                                )
//                            )
//                        }
//                        view1.recycler.adapter =
//                            Expendable_Trade_Wise_Adapter(
//                                activity,
//                                list
//                            )
//
//                        view1.recycler_table.adapter =
//                            Gross_Wise_Adapter(
//                                activity,
//                                list_2
//                            )
//
//                        view1.tv_target_sum.text = roundof(get_valuinlaks((target_sales.toString()))!!).toString()
//                        view1.tv_netsales_sum.text = roundof(get_valuinlaks((total_sales.toString()))!!).toString()
//                        view1.tv_mtd_sum.text = roundof(get_valuinlaks((mtd_Amount.toString()))!!).toString()
//                        view1.tv_lymtd_sum.text = roundof(get_valuinlaks((lymtd_Amount.toString()))!!).toString()
//                        view1.tv_gross_sum.text = roundof(get_valuinlaks((gross_Amount.toString()))!!).toString()
//                        view1.tv_salesreturn_sum.text = roundof(get_valuinlaks((return_Amount.toString()))!!).toString()
//                        tv_growthtotal = (((mtd_Amount-lymtd_Amount )/lymtd_Amount)*100)
//                        view1.tv_growthtotal.text = roundof((tv_growthtotal.toString())!!).toString()
//                    }
//
//                    addchartdata(view1.chart1, entries1)
//
//                    view1.scrollView.visibility = View.VISIBLE
//                    view1.rl_empty.visibility = View.GONE
//
//                }else{
//
//                    view1.scrollView.visibility = View.GONE
//                    view1.rl_empty.visibility = View.VISIBLE
//                    view1.tv_totalsales.setText("₹ 0")
//                }
//
//            }else{
//                view1.scrollView.visibility = View.GONE
//                view1.rl_empty.visibility = View.VISIBLE
//                view1.tv_totalsales.setText("₹ 0")
//            }
////            getgetcoearthdata(fromdate,todate,context)
//            adjust_arraylist(list)
//        }catch (e: Exception) {
//            Log.e("error","",e);
//        }
//    }

    fun setdataonui_sortvalues(it:String,fromdate:String,todate:String,context:Context){
        total_sales = 0.0
        target_sales = 0.0
        mtd_Amount = 0.0
        lymtd_Amount = 0.0
        gross_Amount = 0.0
        return_Amount = 0.0
        tv_growthtotal = 0.0
        try {
            var obj= JSONObject(it)
            if(obj.getString("status").equals("true")) {
                val jsonObject = obj.getJSONObject("data")
                total = jsonObject.getLong("total_Sales")
                if (total > 0){
                    view1.tv_totalsales.setText("₹ "+getFormatedNumber(total.toString()))


//                trade
                    if (jsonObject.getJSONArray("trades").length()>0){
                        var tradelong :Double= 0.0
                        for (j in 0 until jsonObject.getJSONArray("trades").length()) {
                            val jsonObjecttrade = jsonObject.getJSONArray("trades").getJSONObject(j)
                            val  roundvalue = roundof((jsonObjecttrade.getLong("total_Sale_Amount").toFloat()/ total * 100).toString())
                            if (roundvalue>0) {
                                entries1.add(
                                    PieEntry(
                                        roundvalue.toFloat()
                                        , TxtFormate(jsonObjecttrade.getString("trade_Name"))
                                    )
                                )
                            }
                            var index_pos=100
                            if(listofstr.contains(TxtFormate(jsonObjecttrade.getString("trade_Name")))) {
                           index_pos= listofstr.indexOf(TxtFormate(jsonObjecttrade.getString("trade_Name")))
                            }else{
                                index_pos=100
                            }

                                var expendableValue = ExpendableTrade(
                                    TxtFormate(jsonObjecttrade.getString("trade_Name")),
                                    jsonObjecttrade.getString("current_Month_Target"),
                                    jsonObjecttrade.getString("mtd_Amount"),
                                    jsonObjecttrade.getString("lmtd_Amount"),
                                    jsonObjecttrade.getString("growth_Percentage")
                                )

                                if (jsonObjecttrade.getJSONArray("subGroup").length()>0){
                                    sbCaList = ArrayList()
                                    for (i in 0 until jsonObjecttrade.getJSONArray("subGroup").length()) {
                                        var jsonSubCatArray = jsonObjecttrade.getJSONArray("subGroup").getJSONObject(i)
//                                        loggg("sub_tradename",TxtFormate(jsonSubCatArray.getString("trade_Name")))
                                        sbCaList.add(
                                            Trade(
                                                TxtFormate(jsonSubCatArray.getString("trade_Name")),
                                                jsonSubCatArray.getString("current_Month_Target"),
                                                jsonSubCatArray.getString("mtd_Amount"),
                                                jsonSubCatArray.getString("lmtd_Amount"),
                                                jsonSubCatArray.getString("growth_Percentage")
                                            ))
                                    }
                                    loggg("tradetest1",sbCaList.get(0)?.trdename+" - ")

                                    expendableValue.tradelist = sbCaList
//                                    sbCaList_of_list.add(sbCaList)
                                }
                            expendableValue.indexPos =index_pos
                                list.add(expendableValue)
//                        if (list.size>0 && list.get(0).tradelist?.size!!>0){
//                            loggg("tradetest",sbCaList_of_list.get(0).get(0)?.trdename+" - ")
//                        }
                            list_2.add(
                                Gross(
                                    TxtFormate(jsonObjecttrade.getString("trade_Name")),
                                    jsonObjecttrade.getString("gross_Amount"),
                                    jsonObjecttrade.getString("return_Amount"),
                                    jsonObjecttrade.getString("total_Sale_Amount")
                                )
                            )

                            total_sales += jsonObjecttrade.getString("total_Sale_Amount").toDouble()

                            if (!jsonObjecttrade.getString("current_Month_Target").equals("null")){
                                target_sales += jsonObjecttrade.getString("current_Month_Target")
                                    .toDouble()
                            }
                            mtd_Amount += jsonObjecttrade.getString("mtd_Amount").toDouble()
                            lymtd_Amount += jsonObjecttrade.getString("lmtd_Amount").toDouble()
                            gross_Amount += jsonObjecttrade.getString("gross_Amount").toDouble()
                            return_Amount += jsonObjecttrade.getString("return_Amount").toDouble()
                            tv_growthtotal += jsonObjecttrade.getString("growth_Percentage")
                                .toDouble()
                        }

                        if (tradelong!=0.0){
                            entries1.add(
                                PieEntry(
                                    tradelong.toFloat()
                                    , "Other"
                                )
                            )
                        }
                        view1.recycler.adapter =
                            Expendable_Trade_Wise_Adapter(
                                activity,
                                list
                            )

                        view1.recycler_table.adapter =
                            Gross_Wise_Adapter(
                                activity,
                                list_2
                            )

                        view1.tv_target_sum.text = roundof(get_valuinlaks((target_sales.toString()))!!).toString()
                        view1.tv_netsales_sum.text = roundof(get_valuinlaks((total_sales.toString()))!!).toString()
                        view1.tv_mtd_sum.text = roundof(get_valuinlaks((mtd_Amount.toString()))!!).toString()
                        view1.tv_lymtd_sum.text = roundof(get_valuinlaks((lymtd_Amount.toString()))!!).toString()
                        view1.tv_gross_sum.text = roundof(get_valuinlaks((gross_Amount.toString()))!!).toString()
                        view1.tv_salesreturn_sum.text = roundof(get_valuinlaks((return_Amount.toString()))!!).toString()
                        tv_growthtotal = (((mtd_Amount-lymtd_Amount )/lymtd_Amount)*100)
                        view1.tv_growthtotal.text = roundof((tv_growthtotal.toString())!!).toString()
                    }

                    addchartdata(view1.chart1, entries1)

                    view1.scrollView.visibility = View.VISIBLE
                    view1.rl_empty.visibility = View.GONE

                }else{

                    view1.scrollView.visibility = View.GONE
                    view1.rl_empty.visibility = View.VISIBLE
                    view1.tv_totalsales.setText("₹ 0")
                }

            }else{
                view1.scrollView.visibility = View.GONE
                view1.rl_empty.visibility = View.VISIBLE
                view1.tv_totalsales.setText("₹ 0")
            }
//            adjust_arraylist(list)

            Collections.sort(list, Comparator { obj1, obj2 ->
                // ## Ascending order
//                obj1.indexPos.compareToIgnoreCase(obj2.indexPos) // To compare string values
                Integer.valueOf(obj1.indexPos).compareTo(Integer.valueOf(obj2.indexPos)); // To compare integer values

                // ## Descending order
                // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
                // return Integer.valueOf(obj2.empId).compareTo(Integer.valueOf(obj1.empId)); // To compare integer values
            })


            view1.recycler.adapter =
                Expendable_Trade_Wise_Adapter(
                    activity,
                    list
                )
                        getgetcoearthdata(fromdate,todate,context)

        }catch (e: Exception) {
            Log.e("error","",e);
        }
    }


    fun seven_day(string: String): String?{
        var df =
            SimpleDateFormat("dd/MM/yyyy", Locale.US)
        val c = Calendar.getInstance()
        val myDate: Date = df.parse(string)
        c.time = myDate
        c.add(Calendar.DAY_OF_YEAR, -6);
        val newDate = c.getTime()
        var date = df.format(newDate);
        loggg("7date",date)
        loggg("71date", Common.dateof(string))
        loggg("72date", Common.dateafterof(string))

        if (Common.dateof(string).toInt()>6){
            date = date
            view1.tv_weekdate.text = "${datelastof(date)} - ${current_date_formated(1)}"
        }else{

            date = "01${Common.dateafterof(string)}"
            view1.tv_weekdate.text = "01${Common.dateafterof(current_date_formated(1)!!)} - ${current_date_formated(1)}"
        }
        return  date
    }

    fun sendcsv(){
        val csv: String =activity!!.getExternalFilesDir(null)!!.getAbsolutePath().toString() + "/Trade_Wise.csv" // Here csv file name is MyCsvFile.csv

        var writer: CSVWriter? = null
        try {
            writer = CSVWriter(FileWriter(csv))
            val data: MutableList<Array<String>> =
                ArrayList()
            data.add(arrayOf("Trade Name", "Net Sale", "MTD", "LMTD", "Groth (%)"))
            for (i in 0..list.size- 1) {
                data.add(arrayOf(list.get(i).trdename!!,list.get(i).netsales!!,list.get(i).mtd!!,
                    list.get(i).lymtd!!,list.get(i).growth!!))
            }

            writer.writeAll(data) // data is adding to csv
            writer.close()

        } catch (e: IOException) {
            e.printStackTrace()
        }

        val emailIntent = Intent(Intent.ACTION_SEND)
        emailIntent.type = "text/plain"

        val file = File(csv)
        val uri: Uri =   FileProvider.getUriForFile(
            activity!!,
            BuildConfig.APPLICATION_ID, //(use your app signature + ".provider" )
            file);
//        val uri: Uri = Uri.fromFile(file)
        emailIntent.putExtra(Intent.EXTRA_STREAM, uri)

        startActivity(Intent.createChooser(emailIntent, null))
    }


    private fun getgetcoearthdata(fromdate: String, todate: String, context: Context) {

        val pd = DelayedProgressDialog()
        pd.show(fragmentManager!!, "")
        val url = baseurl_managment + "getcoearth?from_date=$fromdate&to_date=$todate"
//       val url = baseurl_managment+"gethome?from_date=01/03/2020&to_date=31/03/2020"
        val stringRequest: StringRequest = object : StringRequest(
            Method.GET, url,
            Response.Listener {
                Common.loggg("getcoearth", it)
                Common.loggg("getcoearth", url)
//                setdataonui(it)
                var jsonObject = JSONObject(it).getJSONObject("data")

//                trade
                if (jsonObject.getJSONArray("trades").length()>0){
                    var tradelong :Double= 0.0
//                    loggg( "stringval11","${jsonObject.getLong("total_Sales")}")
//                  loggg( "stringval",jsonObject.getString("total_Sales").substring(0,jsonObject.getString("total_Sales").lastIndexOf("."))+"  inde=${jsonObject.getString("total_Sales").lastIndexOf(".")}")
                    var totalwithglob=total+jsonObject.getLong("total_Sales")
                    loggg("inputvalue","$totalwithglob")
                    if (totalwithglob.toString().contains("E")){
                        totalwithglob = totalwithglob.toString().replace("E","").toLong()
                    }
                    view1.tv_totalsales.setText("₹ "+getFormatedNumber(totalwithglob.toString()))

                    for (i in 0 until jsonObject.getJSONArray("trades").length()) {
                        val jsonObjecttrade = jsonObject.getJSONArray("trades").getJSONObject(i)

                        val  roundvalue = roundof((jsonObjecttrade.getLong("total_Sale_Amount").toFloat()/ total * 100).toString())

                        if (roundvalue>0) {

                            entries1.add(
                                PieEntry(
                                    roundvalue.toFloat()
                                    , TxtFormate(jsonObjecttrade.getString("trade_Name"))
                                )
                            )

                        }

//                        if (!listofstr.contains(TxtFormate(jsonObjecttrade.getString("trade_Name")))) {
//                            sbCaList = ArrayList()
                        var index_pos=100
                        var isValueexist=false
                        if (listofstr.contains(TxtFormate(jsonObjecttrade.getString("trade_Name")))) {
                            index_pos=     listofstr.indexOf(TxtFormate(jsonObjecttrade.getString("trade_Name")))
                        }else{
                            index_pos=100
                        }

                        var expendableValue = ExpendableTrade(
                            TxtFormate(jsonObjecttrade.getString("trade_Name")),
                            jsonObjecttrade.getString("current_Month_Target"),
                            jsonObjecttrade.getString("mtd_Amount"),
                            jsonObjecttrade.getString("lmtd_Amount"),
                            jsonObjecttrade.getString("growth_Percentage")
                        )

                        for (i in 0 until list.size) {
                            isValueexist =false
                            if (list.get(i).trdename.equals(jsonObjecttrade.getString("trade_Name"))){
                                index_pos = i
                                isValueexist = true
                                list.get(i).netsales = (formateNulltoValue(list.get(i).netsales!!)!!.toFloat()+ formateNulltoValue(jsonObjecttrade.getString("current_Month_Target"))!!.toFloat()).toString()
                                list.get(i).mtd = (list.get(i).mtd!!.toFloat()+  jsonObjecttrade.getString("mtd_Amount").toFloat()).toString()
                                list.get(i).lymtd = ( list.get(i).lymtd!!.toFloat() +jsonObjecttrade.getString("lmtd_Amount").toFloat()).toString()
                                list.get(i).growth = (list.get(i).growth!!.toFloat()+  jsonObjecttrade.getString("growth_Percentage").toFloat()).toString()
                                break
                            }
                        }

                        expendableValue.indexPos =index_pos
                        if (isValueexist) {
                            list.set(index_pos,list.get(index_pos))
                        }else{
                            list.add(expendableValue)

                        }
                        list_2.add(Gross(TxtFormate(jsonObjecttrade.getString("trade_Name")),
                            jsonObjecttrade.getString("gross_Amount"),
                            jsonObjecttrade.getString("return_Amount"), jsonObjecttrade.getString("total_Sale_Amount")))

                        total_sales += jsonObjecttrade.getString("total_Sale_Amount").toDouble()

                        if (!jsonObjecttrade.getString("current_Month_Target").equals("null")){
                            target_sales += jsonObjecttrade.getString("current_Month_Target")
                                .toDouble()
                        }
                        mtd_Amount += jsonObjecttrade.getString("mtd_Amount").toDouble()
                        lymtd_Amount += jsonObjecttrade.getString("lmtd_Amount").toDouble()
                        gross_Amount += jsonObjecttrade.getString("gross_Amount").toDouble()
                        return_Amount += jsonObjecttrade.getString("return_Amount").toDouble()
                        tv_growthtotal += jsonObjecttrade.getString("growth_Percentage").toDouble()
                    }

                    if (tradelong!=0.0){
                        entries1.add(
                            PieEntry(
                                tradelong.toFloat()
                                , "Other"
                            )
                        )
                    }
//                    val sortedAppsList=     list.sortedBy { it.trdename }
//                    adjust_arraylist(list)

//                  list_2.sortBy { it.trdename }

                    Collections.sort(list, Comparator { obj1, obj2 ->
                        // ## Ascending order
//                obj1.indexPos.compareToIgnoreCase(obj2.indexPos) // To compare string values
                        Integer.valueOf(obj1.indexPos).compareTo(Integer.valueOf(obj2.indexPos)); // To compare integer values

                        // ## Descending order
                        // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
                        // return Integer.valueOf(obj2.empId).compareTo(Integer.valueOf(obj1.empId)); // To compare integer values
                    })


                    view1.recycler.adapter =
                        Expendable_Trade_Wise_Adapter(
                            activity,
                            list
                        )

                    view1.recycler_table.adapter =
                        Gross_Wise_Adapter(
                            activity,
                            list_2
                        )

                    view1.tv_target_sum.text = roundof(get_valuinlaks((target_sales.toString()))!!).toString()
                    view1.tv_netsales_sum.text = roundof(get_valuinlaks((total_sales.toString()))!!).toString()
                    view1.tv_mtd_sum.text = roundof(get_valuinlaks((mtd_Amount.toString()))!!).toString()
                    view1.tv_lymtd_sum.text = roundof(get_valuinlaks((lymtd_Amount.toString()))!!).toString()
                    view1.tv_gross_sum.text = roundof(get_valuinlaks((gross_Amount.toString()))!!).toString()
                    view1.tv_salesreturn_sum.text = roundof(get_valuinlaks((return_Amount.toString()))!!).toString()
                    tv_growthtotal = (((mtd_Amount-lymtd_Amount )/lymtd_Amount)*100)
                    loggg("tv_growthtotal","$tv_growthtotal")
                    view1.tv_growthtotal.text = roundof((tv_growthtotal.toString())).toString()
                }


                pd.cancel()

            },
            Response.ErrorListener {
                pd.cancel()
                Common.toast(context, Common.volleyerror(it))
            }) {
            override fun getParams(): Map<String, String> {
                val params2: MutableMap<String, String> = java.util.HashMap()

//                params2["name"]= "${personName}"
                return params2
            }

            override fun getHeaders(): MutableMap<String, String> {
                val params2: MutableMap<String, String> = java.util.HashMap()
                params2.put(
                    "Authorization",
                    "Bearer ${UserSharedPrefrences.getInstance(context).gettoken()}"
                )
                return params2
            }
        }
        stringRequest.setShouldCache(false)
        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        stringRequest.retryPolicy = policy
        MySingleton.getInstance(context).addToRequestQueue(stringRequest)
    }


//    fun adjust_arraylist(list: ArrayList<ExpendableTrade>){
//        var siz=list_new.size
//        var pos=0
//        while (siz>pos){
////            loggg("inn","$siz  , $pos")
//            if (list_new.get(pos).netsales==null){
//                list_new.removeAt(pos)
//                siz--
//            }else{
//                pos++
//            }
//        }
//        list_new.addAll(list)
//        view1.recycler.adapter =
//            Expendable_Trade_Wise_Adapter(
//                activity,
//                list_new
//            )
//    }

//    private fun addvalues() {
//        list_new.add(ExpendableTrade())
//        list_new.add(ExpendableTrade())
//        list_new.add(ExpendableTrade())
//        list_new.add(ExpendableTrade())
//        list_new.add(ExpendableTrade())
//        list_new.add(ExpendableTrade())
//        list_new.add(ExpendableTrade())
//        list_new.add(ExpendableTrade())
//        list_new.add(ExpendableTrade())
//        list_new.add(ExpendableTrade())
//        list_new.add(ExpendableTrade())
//        list_new.add(ExpendableTrade())
//        list_new.add(ExpendableTrade())
//        list_new.add(ExpendableTrade())
//        list_new.add(ExpendableTrade())
//        list_new.add(ExpendableTrade())
//        list_new.add(ExpendableTrade())
//        list_new.add(ExpendableTrade())
//        list_new.add(ExpendableTrade())
//        list_new.add(ExpendableTrade())
//        list_new.add(ExpendableTrade())
//        list_new.add(ExpendableTrade())
//        list_new.add(ExpendableTrade())
//        list_new.add(ExpendableTrade())
//        list_new.add(ExpendableTrade())
//    }
}