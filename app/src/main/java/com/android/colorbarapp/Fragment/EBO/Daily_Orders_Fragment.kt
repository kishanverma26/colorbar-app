package com.android.colorbarapp.Fragment.EBO

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.colorbarapp.Adapter.Daily_orders_Adapter
import com.android.colorbarapp.CustomClasses.DelayedProgressDialog
import com.android.colorbarapp.CustomClasses.MySingleton
import com.android.colorbarapp.Model.Store
import com.android.colorbarapp.R
import com.android.colorbarapp.Utils.ParamKeys
import com.android.colorbarapp.Utils.UserSharedPrefrences
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.request.StringRequest
import com.s.strokeclock.Utils.Common
import com.s.strokeclock.Utils.Common.Companion.getFormatedNumber
import io.paperdb.Paper
import kotlinx.android.synthetic.main.daily_order_fragmnet.view.*
import kotlinx.android.synthetic.main.header_topbar.view.*
import org.json.JSONObject

class Daily_Orders_Fragment : Fragment() , ParamKeys {

    lateinit var mcoxt: Context
    lateinit var view1: View
    lateinit var list: ArrayList<Store>
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.daily_order_fragmnet,container,false)
        mcoxt= activity!!
        view1 = view
        initi();
        return view
    }

    private fun initi() {
        list = ArrayList()


        view1.recycler.layoutManager = LinearLayoutManager(activity)
        view1.recycler.adapter = Daily_orders_Adapter(activity,list)
        view1.tvusername.setText(UserSharedPrefrences.getInstance(activity!!).getname())
        view1. iv_icon.setOnClickListener { v -> Common.popup_Menu(activity!!,activity!!,view1. iv_icon) }

        if (Paper.book(EBOproductBook).read<String>(Dailyorderres)!=null){
            setup_tabledata_ui(Paper.book(EBOproductBook).read<String>(Dailyorderres))
        }

        getdata(activity!!)

    }


    private fun getdata(context: Context) {


        val pd= DelayedProgressDialog()
        pd.show(fragmentManager!!,"")
        val url = baseurl_ebo+"getdailysalesreport"

        val stringRequest: StringRequest = object: StringRequest(Method.GET,url,
                Response.Listener {
                    Common.loggg("getdailysalesreport", it)
                    Common.loggg("url", url)
                    Paper.book(EBOproductBook).write(Dailyorderres,it)

                    setup_tabledata_ui(it)
                    pd.cancel()

                },
                Response.ErrorListener {
                    pd.cancel()
                    Common.toast(context, Common.volleyerror(it))
                })
        {
            override  fun getParams(): Map<String, String>  {
                val params2: MutableMap<String, String> = HashMap()

//                params2["name"]= "${personName}"
                return params2
            }

            override fun getHeaders(): MutableMap<String, String> {
                val params2: MutableMap<String, String> = HashMap()
                params2.put("Authorization","Bearer ${UserSharedPrefrences.getInstance(context).gettoken()}")
                return params2
            }

        }
        stringRequest.setShouldCache(false)
        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy
        MySingleton.getInstance(context).addToRequestQueue(stringRequest)
    }

    var total_sales = 0.0
    private fun setup_tabledata_ui(it: String?) {
        list.clear()
        total_sales = 0.0
        try {


            var obj= JSONObject(it)
            if(obj.getString("status").equals("true")) {
                val jsonObject = obj.getJSONArray("data")

                if (jsonObject.length() > 0){


//                category

                    for (i in 0..jsonObject.length() - 1) {
                        val jsonObjecttrade = jsonObject.getJSONObject(i)

                        list.add(
                                Store("",Common.TxtFormate( jsonObjecttrade.getString("revenue")),
                                        Common.roundofstring(jsonObjecttrade.getString("date"))))

                        total_sales =  total_sales + jsonObjecttrade.getString("revenue").toDouble()
                    }

                    view1.recycler.adapter!!.notifyDataSetChanged()

                   view1. tv_totalsales.text =getFormatedNumber(total_sales.toString())
//                    view1.scrollView.visibility = View.VISIBLE
//                    view1.rl_empty.visibility = View.GONE

                }else{
//
//                    view1.scrollView.visibility = View.GONE
//                    view1.rl_empty.visibility = View.VISIBLE

                }

            }else{
//                view1.scrollView.visibility = View.GONE
//                view1.rl_empty.visibility = View.VISIBLE

            }

        }catch (e: Exception) {

            Log.e("error","",e);
        }
    }


}