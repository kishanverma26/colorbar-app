package com.android.colorbarapp.Fragment.EBO

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.transition.Slide
import androidx.transition.Transition
import androidx.transition.TransitionManager
import com.android.colorbarapp.Adapter.EBO.Product_Adapter
import com.android.colorbarapp.CustomClasses.DelayedProgressDialog
import com.android.colorbarapp.CustomClasses.MySingleton
import com.android.colorbarapp.Model.Product
import com.android.colorbarapp.R
import com.android.colorbarapp.Utils.ParamKeys
import com.android.colorbarapp.Utils.UserSharedPrefrences
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.request.StringRequest
import com.archit.calendardaterangepicker.customviews.CalendarListener
import com.archit.calendardaterangepicker.customviews.DateRangeCalendarViewApi
import com.s.strokeclock.Utils.Common
import com.s.strokeclock.Utils.Common.Companion.getFormatedNumber
import io.paperdb.Paper
import kotlinx.android.synthetic.main.calender_view.view.*
import kotlinx.android.synthetic.main.header_topbar.view.*
import kotlinx.android.synthetic.main.product_report_fragmnet.view.*
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class Product_Report_Fragment : Fragment() ,View.OnClickListener , ParamKeys {

    lateinit var mcoxt: Context
    lateinit var view1: View
    lateinit var list: ArrayList<Product>
    var strcaldate = ""

    var calenderpos = 0   // 0 = M , 1=Week , 2 = Day
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.product_report_fragmnet,container,false)
        mcoxt= activity!!
        view1 = view
        initi();
        return view
    }

    private fun initi() {
        list = ArrayList()


        strcaldate = current_date()!!
        view1.ivcalender.setOnClickListener(this)
        view1.tv_day.setOnClickListener(this)
        view1.tv_week.setOnClickListener(this)
        view1.tv_month.setOnClickListener(this)

        view1.recycler.layoutManager = LinearLayoutManager(activity)
        view1.recycler.adapter = Product_Adapter(activity, list)
        view1.tvusername.setText(UserSharedPrefrences.getInstance(activity!!).getname())
        view1. iv_icon.setOnClickListener { v -> Common.popup_Menu(activity!!,activity!!,view1. iv_icon) }


        setupcalenderview()

        if (Paper.book(EBOproductBook).read<String>(EboKeyresonse)!=null){
            setup_tabledata_ui(Paper.book(EBOproductBook).read<String>(EboKeyresonse))
        }
        view1.tv_monthdate.text = Common.current_date_formated(2)
//        getdata("01/03/2020","31/03/2020",activity!!)

        getdata(Common.one_month(strcaldate).toString(),strcaldate,activity!!)
    }


    var strcalenderdate1=""
    var strcalenderdate2=""

    private fun setupcalenderview() {
        view1.  tvclose.setOnClickListener(this)
        view1. tvok.setOnClickListener(this)

        view1. clcalendar.setCalendarListener(object : CalendarListener {

            override fun onDateRangeSelected(startDate: Calendar, endDate: Calendar) {
                val df =
                        SimpleDateFormat("dd/MM/yyyy", Locale.US)
                Toast.makeText(
                        activity,
                        "Start Date: " +  df.format(startDate.time)
                                .toString() + " End date: " +df.format( endDate.time).toString(),
                        Toast.LENGTH_SHORT
                ).show()

                strcalenderdate1 =df.format(startDate.time)
                strcalenderdate2 =df.format(endDate.time)

            }

            override fun onFirstDateSelected(startDate: Calendar) {
                val df =
                        SimpleDateFormat("dd/MM/yyyy", Locale.US)
                strcalenderdate1 =df.format(startDate.time)
            }
        })


        val startMonth = Calendar.getInstance()
        startMonth.add(Calendar.YEAR, -20)
        view1. clcalendar.setVisibleMonthRange(startMonth, Calendar.getInstance())
        view1. clcalendar.setSelectableDateRange(startMonth, Calendar.getInstance())
        view1. clcalendar.setCurrentMonth(Calendar.getInstance())
//        view1. clcalendar.setSelectedDateRange(startSelectedDate,  Calendar.getInstance());
    }

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.ivcalender->{
                shocalender()
                strcalenderdate1 =""
                strcalenderdate2 =""

            }
            R.id.tvclose->{

                hidecalender()
            }
            R.id.tvok->{
                view1. clcalendar.resetAllSelectedViews()
                if (strcalenderdate1.length>0) {
                hidecalender()
                showcalenderdate(View.VISIBLE)
                if (strcalenderdate2.length>0){

                    getdata(strcalenderdate1,strcalenderdate2,activity!!)

                    view1.tv_calenderdate.text = "${Common.datelastof(strcalenderdate1)} - ${Common.datelastof(strcalenderdate2)}"
                }else{
                    getdata(strcalenderdate1,strcalenderdate1,activity!!)

                    view1.tv_calenderdate.text = "${Common.datelastof(strcalenderdate1)}"
                }
                view1.tv_day.setTextColor(ContextCompat.getColor(activity!!,R.color.bottomsheet_blue))
                view1.tv_day.background = null
                view1.tv_week.setTextColor(ContextCompat.getColor(activity!!,R.color.bottomsheet_blue))
                view1.tv_week.background = null
                view1.tv_month.setTextColor(ContextCompat.getColor(activity!!,R.color.bottomsheet_blue))
                view1.tv_month.background = null

                view1.tv_daydate.visibility = View.GONE
                view1.tv_weekdate.visibility = View.VISIBLE
                view1.tv_monthdate.visibility = View.GONE
                }else{
                    Common.toast(activity!!, "Please select date")
                }

            }

            R.id.tv_day->{
                calenderpos =2
//                getdata("04/03/2020","04/03/2020",activity!!)


              getdata(current_date().toString(),current_date().toString(),activity!!)
                changecolor(view1.tv_day,view1.tv_week,view1.tv_month)
                view1.tv_weekdate.visibility = View.GONE
                view1.tv_monthdate.visibility = View.GONE
                view1.tv_daydate.visibility = View.VISIBLE
              view1.tv_daydate.text = current_date_formated(0)
//                view1.tv_daydate.text ="08 Mar"
                showcalenderdate(View.GONE)
            }
            R.id.tv_week->{
                calenderpos =1
//                getdata("01/03/2020","07/03/2020",activity!!)
              getdata(seven_day(strcaldate).toString(),strcaldate,activity!!)

                changecolor(view1.tv_week,view1.tv_day,view1.tv_month)

                view1.tv_monthdate.visibility = View.GONE
                view1.tv_daydate.visibility = View.GONE
                view1.tv_weekdate.visibility = View.VISIBLE
//              seven_day("07/03/2020")
//                view1.tv_weekdate.text = "1/03 - 7/03"
                showcalenderdate(View.GONE)
            }
            R.id.tv_month->{
                calenderpos =0
//                getdata("01/03/2020","31/03/2020",activity!!)
              getdata(Common.one_month(strcaldate).toString(),strcaldate,activity!!)

                changecolor(view1.tv_month,view1.tv_day,view1.tv_week)

                view1.tv_daydate.visibility = View.GONE
                view1.tv_weekdate.visibility = View.GONE
                view1.tv_monthdate.visibility = View.VISIBLE
              view1.tv_monthdate.text = current_date_formated(2)
//                view1.tv_monthdate.text ="Mar"

                showcalenderdate(View.GONE)
            }

        }
    }
    fun showcalenderdate(vale:Int){
        if (vale==View.VISIBLE) {
            view1.lldate.visibility = View.GONE
            view1.tv_calenderdate.visibility = View.VISIBLE
        }else{
            view1.lldate.visibility = View.VISIBLE
            view1.tv_calenderdate.visibility = View.GONE
        }
    }
    fun changecolor(textView: TextView, textView1: TextView, textView2: TextView){
        textView.setTextColor(ContextCompat.getColor(activity!!,R.color.white))
        textView.background = ContextCompat.getDrawable(activity!!,R.drawable.selected_tab_bg)

        textView1.setTextColor(ContextCompat.getColor(activity!!,R.color.bottomsheet_blue))
        textView1.background = null

        textView2.setTextColor(ContextCompat.getColor(activity!!,R.color.bottomsheet_blue))
        textView2.background = null



    }


    fun shocalender(){
        val transition: Transition = Slide(Gravity.RIGHT)
        transition.setDuration(600)
        transition.addTarget(view1.view_calender)
        TransitionManager.beginDelayedTransition(view1.rlparant, transition);
        view1.view_calender.visibility = View.VISIBLE

    }
    fun hidecalender(){
        val transition: Transition = Slide(Gravity.RIGHT)
        transition.setDuration(600)
        transition.addTarget(view1.view_calender)
        TransitionManager.beginDelayedTransition(view1.rlparant, transition);
        view1.view_calender.visibility = View.GONE


    }

    private fun current_date(): String? {
        val c = Calendar.getInstance()
        val df =
                SimpleDateFormat("dd/MM/yyyy", Locale.US)
        return df.format(c.time)
    }

    private fun current_date_formated(position: Int): String? {
        var df =SimpleDateFormat("dd MMM", Locale.US)
        val c = Calendar.getInstance()
//        println("Current time => " + c.time)
        if (position==0) {
            df =
                    SimpleDateFormat("dd MMM", Locale.US)
        }else if (position==1){
            df =
                    SimpleDateFormat("dd/MM", Locale.US)
        }else if (position==2){
            df =
                    SimpleDateFormat("MMM", Locale.US)
        }
        return df.format(c.time)
    }



    fun seven_day(string: String): String?{
        var df =
                SimpleDateFormat("dd/MM/yyyy", Locale.US)
        val c = Calendar.getInstance()
        val myDate: Date = df.parse(string)
        c.time = myDate
        c.add(Calendar.DAY_OF_YEAR, -7);
        val newDate = c.getTime()
        var date = df.format(newDate);
        Common.loggg("7date", date)
        Common.loggg("71date", Common.dateof(string))
        Common.loggg("72date", Common.dateafterof(string))

        if (Common.dateof(string).toInt()>6){
            date = date
            view1.tv_weekdate.text = "${Common.datelastof(date)} - ${current_date_formated(1)}"
        }else{

            date = "01${Common.dateafterof(string)}"
            view1.tv_weekdate.text = "01${Common.dateafterof(current_date_formated(1)!!)} - ${current_date_formated(1)}"
        }
        return  date
    }



    private fun getdata(fromdate:String,todate:String,context: Context) {





        val pd= DelayedProgressDialog()
        pd.show(fragmentManager!!,"")
        val url = baseurl_ebo+"getproducts?from_date=$fromdate&to_date=$todate"
        val stringRequest: StringRequest = object:StringRequest(Method.GET,url,
                Response.Listener {
                    Common.loggg("gethome", it)
                    Common.loggg("url", url)
                    Paper.book(EBOproductBook).write(EboKeyresonse,it)
                    setup_tabledata_ui(it)
                    pd.cancel()

                },
                Response.ErrorListener {
                    pd.cancel()
                    Common.toast(context, Common.volleyerror(it))
                })
        {
            override  fun getParams(): Map<String, String>  {
                val params2: MutableMap<String, String> = HashMap()

//                params2["name"]= "${personName}"
                return params2
            }
            override fun getHeaders(): MutableMap<String, String> {
                val params2: MutableMap<String, String> = HashMap()
                params2.put("Authorization","Bearer ${UserSharedPrefrences.getInstance(context).gettoken()}")
                return params2
            }
        }
        stringRequest.setShouldCache(false)
        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy
        MySingleton.getInstance(context).addToRequestQueue(stringRequest)
    }

    var total_sales = 0.0
    private fun setup_tabledata_ui(it: String?) {
        list.clear()
        total_sales = 0.0
        try {


            var obj= JSONObject(it)
            if(obj.getString("status").equals("true")) {
                val jsonObject = obj.getJSONArray("data")

                if (jsonObject.length() > 0){


//                category

                        for (i in 0..jsonObject.length() - 1) {
                            val jsonObjecttrade = jsonObject.getJSONObject(i)

                            list.add(
                                    Product(
                                            Common.TxtFormate(jsonObjecttrade.getString("productName")),
                                            Common.TxtFormate( jsonObjecttrade.getString("categoryName")),
                                            Common.roundofstring(jsonObjecttrade.getString("quantity")),
                                            Common.roundofstring(jsonObjecttrade.getString("amount")))
                            )
                            total_sales =  total_sales + jsonObjecttrade.getString("amount").toDouble()
                        }

                    view1.recycler.adapter!!.notifyDataSetChanged()
//                    view1. tv_totalsales.text = String.format("%,d",total_sales.toLong())
              view1. tv_totalsales.text = getFormatedNumber(total_sales.toString())

                    view1.scrollView.visibility = View.VISIBLE
                    view1.rl_empty.visibility = View.GONE

                }else{

                    view1.scrollView.visibility = View.GONE
                    view1.rl_empty.visibility = View.VISIBLE

                }

            }else{
                view1.scrollView.visibility = View.GONE
                view1.rl_empty.visibility = View.VISIBLE

            }

        }catch (e: Exception) {

            Log.e("error","",e);
        }
    }




}