package com.android.colorbarapp.Fragment.Managment

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.LinearGradient
import android.graphics.Shader
import android.net.Uri
import android.os.Bundle
import android.text.TextPaint
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.transition.Slide
import androidx.transition.Transition
import androidx.transition.TransitionManager
import com.android.colorbarapp.Adapter.Managment.Category_Horizontal_Adapter
import com.android.colorbarapp.Adapter.Managment.Category_wise_Adapter
import com.android.colorbarapp.Adapter.Managment.Regin_Wise_Adapter
import com.android.colorbarapp.Adapter.Managment.Total_Category_Wise_Adapter
import com.android.colorbarapp.BuildConfig
import com.android.colorbarapp.CustomClasses.DelayedProgressDialog
import com.android.colorbarapp.CustomClasses.MySingleton
import com.android.colorbarapp.Interfaces.TabClick
import com.android.colorbarapp.Model.Category
import com.android.colorbarapp.Model.Region
import com.android.colorbarapp.Model.TradeTotal
import com.android.colorbarapp.R
import com.android.colorbarapp.Utils.ParamKeys
import com.android.colorbarapp.Utils.UserSharedPrefrences
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.request.StringRequest
import com.archit.calendardaterangepicker.customviews.CalendarListener
import com.archit.calendardaterangepicker.customviews.DateRangeCalendarViewApi
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.utils.MPPointF
import com.opencsv.CSVWriter
import com.s.strokeclock.Utils.Common
import com.s.strokeclock.Utils.Common.Companion.current_date_formated
import com.s.strokeclock.Utils.Common.Companion.getFormatedNumber
import com.s.strokeclock.Utils.Common.Companion.loggg
import com.s.strokeclock.Utils.Common.Companion.roundof
import com.s.strokeclock.Utils.Common.Companion.roundofstring
import io.paperdb.Paper
import kotlinx.android.synthetic.main.calender_view.view.*
import kotlinx.android.synthetic.main.category_wise_fragment.view.*
import kotlinx.android.synthetic.main.category_wise_fragment.view.chart1
import kotlinx.android.synthetic.main.category_wise_fragment.view.iv_detail
import kotlinx.android.synthetic.main.category_wise_fragment.view.ivcalender
import kotlinx.android.synthetic.main.category_wise_fragment.view.lldate
import kotlinx.android.synthetic.main.category_wise_fragment.view.recycler
import kotlinx.android.synthetic.main.category_wise_fragment.view.recyclercat
import kotlinx.android.synthetic.main.category_wise_fragment.view.rl_empty
import kotlinx.android.synthetic.main.category_wise_fragment.view.rlparant
import kotlinx.android.synthetic.main.category_wise_fragment.view.scrollView
import kotlinx.android.synthetic.main.category_wise_fragment.view.tv_calenderdate
import kotlinx.android.synthetic.main.category_wise_fragment.view.tv_day
import kotlinx.android.synthetic.main.category_wise_fragment.view.tv_daydate
import kotlinx.android.synthetic.main.category_wise_fragment.view.tv_month
import kotlinx.android.synthetic.main.category_wise_fragment.view.tv_monthdate
import kotlinx.android.synthetic.main.category_wise_fragment.view.tv_totalsales
import kotlinx.android.synthetic.main.category_wise_fragment.view.tv_week
import kotlinx.android.synthetic.main.category_wise_fragment.view.tv_weekdate
import kotlinx.android.synthetic.main.category_wise_fragment.view.tvshare
import kotlinx.android.synthetic.main.category_wise_fragment.view.view_calender
import kotlinx.android.synthetic.main.header_topbar.view.*
import kotlinx.android.synthetic.main.region_wise_fragmnet.view.*
import kotlinx.android.synthetic.main.total_sales_fragment.view.*
import kotlinx.android.synthetic.main.trade_wise_fragment.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class Category_Wise_Fragment : Fragment() ,View.OnClickListener , ParamKeys ,TabClick{

    lateinit var mcoxt: Context
    lateinit var view1: View
    lateinit var list: ArrayList<Category>
    lateinit var list2: ArrayList<Category>
//    var entries1 = ArrayList<PieEntry>()
    var spinnervalue = ""
    var strcaldate = ""
    var calenderpos = 0   // 0 = M , 1=Week , 2 = Day

    val progressDialog= DelayedProgressDialog()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.category_wise_fragment,container,false)
        mcoxt= activity!!
        view1 = view

        initi()


        progressDialog.isCancelable = false
        return view
    }
    fun showcalenderdate(vale:Int){
        if (vale==View.VISIBLE) {
            view1.lldate.visibility = View.GONE
            view1.tv_calenderdate.visibility = View.VISIBLE
        }else{
            view1.lldate.visibility = View.VISIBLE
            view1.tv_calenderdate.visibility = View.GONE
        }
    }
    private fun initi() {
        strcaldate = Common.current_date()!!
        list = ArrayList()
        list2 = ArrayList()

        val paint: TextPaint = view1.tvcategory.getPaint()
        val width: Float = paint.measureText("Category Wise")

        val textShader: Shader = LinearGradient(
            0F, 0F, width, view1.tvcategory.getTextSize(), intArrayOf(
                Color.parseColor("#5b3e3e"),
                Color.parseColor("#ac8686"),
                Color.parseColor("#5b3e3e")
            ), null, Shader.TileMode.CLAMP
        )
        view1.tvcategory.getPaint().setShader(textShader)

        val paint2: TextPaint = view1.tvmanag.getPaint()
        val width2: Float = paint2.measureText("Management Report Summary")

        val textShader2: Shader = LinearGradient(
            0F, 0F, width2, view1.tvmanag.getTextSize(), intArrayOf(
                Color.parseColor("#5b3e3e"),
                Color.parseColor("#ac8686"),
                Color.parseColor("#5b3e3e")
            ), null, Shader.TileMode.CLAMP
        )
        view1.tvmanag.getPaint().setShader(textShader2)

        view1.tvshare.setOnClickListener(this)
        view1.ivcalender.setOnClickListener(this)
        view1.tv_day.setOnClickListener(this)
        view1.tv_week.setOnClickListener(this)
        view1.tv_month.setOnClickListener(this)

        view1.recycler.layoutManager = LinearLayoutManager(activity)
        view1.recycler.isNestedScrollingEnabled = false
        view1.iv_detail.setOnClickListener(this)

        view1.tvusername.setText(UserSharedPrefrences.getInstance(activity!!).getname())
        view1. iv_icon.setOnClickListener { v -> Common.popup_Menu(activity!!,activity!!,view1. iv_icon) }

        setupcalenderview()
        progressDialog.show(fragmentManager!!,"")

        view1.tv_monthdate.text = Common.current_date_formated(2)
        if (Paper.book(TradeNameBook).read<String>(TradeNameres)!=null){
            setspinnerdata_onui(Paper.book(TradeNameBook).read<String>(TradeNameres))
        }else{
            getdataspinner(activity!!)
        }


    }

    var strcalenderdate1=""
    var strcalenderdate2=""

    private fun setupcalenderview() {
        view1.  tvclose.setOnClickListener(this)
        view1. tvok.setOnClickListener(this)

        view1. clcalendar.setCalendarListener(object : CalendarListener {

            override fun onDateRangeSelected(startDate: Calendar, endDate: Calendar) {
                val df =
                        SimpleDateFormat("dd/MM/yyyy", Locale.US)
                Toast.makeText(
                        activity,
                        "Start Date: " +  df.format(startDate.time)
                                .toString() + " End date: " +df.format( endDate.time).toString(),
                        Toast.LENGTH_SHORT
                ).show()

                strcalenderdate1 =df.format(startDate.time)
                strcalenderdate2 =df.format(endDate.time)

            }

            override fun onFirstDateSelected(startDate: Calendar) {
                val df =
                        SimpleDateFormat("dd/MM/yyyy", Locale.US)
                strcalenderdate1 =df.format(startDate.time)
            }
        })


        val startMonth = Calendar.getInstance()
        startMonth.add(Calendar.YEAR, -20)
        view1. clcalendar.setVisibleMonthRange(startMonth, Calendar.getInstance())
        view1. clcalendar.setSelectableDateRange(startMonth, Calendar.getInstance())
        view1. clcalendar.setCurrentMonth(Calendar.getInstance())
//        view1. clcalendar.setSelectedDateRange(startSelectedDate,  Calendar.getInstance());
    }

    fun addchartdata(chartnew : PieChart,entries : ArrayList<PieEntry>){
        chartnew.setUsePercentValues(true)
        chartnew.setHoleRadius(50f)
        chartnew.setTransparentCircleColor(R.color.colorAccent)
        chartnew.setTransparentCircleAlpha(10)
//        chartnew.setExtraOffsets(-100f,100f,0f,0f)
        chartnew.setHoleColor(12176592)
        chartnew.getDescription().setEnabled(false)
        chartnew.legend.isEnabled = false
        chartnew.legend.isWordWrapEnabled = true
        chartnew.setEntryLabelColor(R.color.bottom_icon_color)
        chartnew.setEntryLabelTextSize(12f)
        chartnew.setDrawEntryLabels(true)

        chartnew.setExtraTopOffset(15f);
        chartnew.setExtraBottomOffset(15f)
        chartnew.setExtraLeftOffset(10f)
        chartnew.setExtraRightOffset(10f)


        var dataSet = PieDataSet(entries, "")

        dataSet.setDrawIcons(false)
        dataSet.setDrawValues(true)



        dataSet.setSliceSpace(2f)
        dataSet.setIconsOffset(MPPointF(0F, 40F))
        dataSet.setSelectionShift(10f)
        val colorFirst = let { ContextCompat.getColor(activity!!, R.color.grapc1) }
        val colorFirst2 = let { ContextCompat.getColor(activity!!, R.color.grapc2) }
        val colorFirst3 = let { ContextCompat.getColor(activity!!, R.color.grapc3) }
        val colorFirst4 = let { ContextCompat.getColor(activity!!, R.color.grapc4) }
        val colorFirst5 = let { ContextCompat.getColor(activity!!, R.color.grapc5) }
        val colorFirst6 = let { ContextCompat.getColor(activity!!, R.color.grapc6) }

        val legends=  chartnew.legend
        legends.verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
        legends.horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT
        legends.orientation = Legend.LegendOrientation.VERTICAL


        val colorFirstlist  = mutableListOf(colorFirst,colorFirst2,colorFirst3,colorFirst4,colorFirst5,colorFirst6)

        dataSet.colors =colorFirstlist

        dataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        dataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
//        dataSet.setValueLinePart1OffsetPercentage(10f);
        dataSet.setValueLinePart1Length(.43f);
        dataSet.setValueLinePart2Length(.4f);

        //dataSet.setSelectionShift(0f);
        val data = PieData(dataSet)
        data.setValueFormatter(PercentFormatter(chartnew))

        data.setValueTextSize(12f)
        data.setValueTextColor(R.color.bottomsheet_blue)

        chartnew.setData(data)
        chartnew.invalidate()
    }

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.tvshare->{
                sendcsv()
            }
            R.id.ivcalender->{
                shocalender()
                strcalenderdate1 =""
                strcalenderdate2 =""

            }
            R.id.tvclose->{

                hidecalender()
            }
            R.id.tvok->{
                view1. clcalendar.resetAllSelectedViews()
                if (strcalenderdate1.length>0) {
                hidecalender()
                showcalenderdate(View.VISIBLE)
                if (strcalenderdate2.length>0){

                    getdata(strcalenderdate1,strcalenderdate2,activity!!)

                    view1.tv_calenderdate.text = "${Common.datelastof(strcalenderdate1)} - ${Common.datelastof(
                            strcalenderdate2
                    )}"
                }else{
                    getdata(strcalenderdate1,strcalenderdate1,activity!!)

                    view1.tv_calenderdate.text = "${Common.datelastof(strcalenderdate1)}"
                }
                view1.tv_day.setTextColor(ContextCompat.getColor(activity!!,R.color.bottomsheet_blue))
                view1.tv_day.background = null
                view1.tv_week.setTextColor(ContextCompat.getColor(activity!!,R.color.bottomsheet_blue))
                view1.tv_week.background = null
                view1.tv_month.setTextColor(ContextCompat.getColor(activity!!,R.color.bottomsheet_blue))
                view1.tv_month.background = null

                view1.tv_daydate.visibility = View.GONE
                view1.tv_weekdate.visibility = View.VISIBLE
                view1.tv_monthdate.visibility = View.GONE

                }else{
                    Common.toast(activity!!, "Please select date")
                }
            }

            R.id.tv_day->{
                calenderpos =2
//                getdata("04/03/2020","04/03/2020",activity!!)


              getdata(Common.current_date().toString(),Common.current_date().toString(),activity!!)
                changecolor(view1.tv_day,view1.tv_week,view1.tv_month)
                view1.tv_weekdate.visibility = View.GONE
                view1.tv_monthdate.visibility = View.GONE
                view1.tv_daydate.visibility = View.VISIBLE
              view1.tv_daydate.text = current_date_formated(0)
//                view1.tv_daydate.text ="08 Mar"
                showcalenderdate(View.GONE)
            }
            R.id.tv_week->{
                calenderpos =1
//                getdata("01/03/2020","07/03/2020",activity!!)
              getdata(seven_day(strcaldate).toString(),strcaldate,activity!!)

                changecolor(view1.tv_week,view1.tv_day,view1.tv_month)

                view1.tv_monthdate.visibility = View.GONE
                view1.tv_daydate.visibility = View.GONE
                view1.tv_weekdate.visibility = View.VISIBLE
//              seven_day("07/03/2020")
//                view1.tv_weekdate.text = "1/03 - 7/03"
                showcalenderdate(View.GONE)
            }
            R.id.tv_month->{
                calenderpos =0
//                getdata("01/03/2020","31/03/2020",activity!!)
              getdata(Common.one_month(strcaldate).toString(),strcaldate,activity!!)

                changecolor(view1.tv_month,view1.tv_day,view1.tv_week)

                view1.tv_daydate.visibility = View.GONE
                view1.tv_weekdate.visibility = View.GONE
                view1.tv_monthdate.visibility = View.VISIBLE
              view1.tv_monthdate.text = current_date_formated(2)
//                view1.tv_monthdate.text ="Mar"

                showcalenderdate(View.GONE)
            }

        }
    }


    private fun getdataspinner(context: Context) {





        val url = baseurl_managment+"gettradenames"
//       val url = baseurl_managment+"gethome?from_date=01/03/2020&to_date=31/03/2020"
        val stringRequest: StringRequest = object: StringRequest(
                Method.GET,url,
                Response.Listener {
                    Common.loggg("gettradenames", it)
                    Common.loggg("url", url)
                    Paper.book(TradeNameBook).write(TradeNameres,it)


                    setspinnerdata_onui(it)
                },
                Response.ErrorListener {
                    progressDialog.cancel()
                    Common.toast(context, Common.volleyerror(it))
                })
        {
            override  fun getParams(): Map<String, String>  {
                val params2: MutableMap<String, String> = HashMap()

//                params2["name"]= "${personName}"
                return params2
            }
            override fun getHeaders(): MutableMap<String, String> {
                val params2: MutableMap<String, String> = HashMap()
                params2.put("Authorization","Bearer ${UserSharedPrefrences.getInstance(context).gettoken()}")
                loggg("header",params2.toString())
                return params2
            }
        }
        stringRequest.setShouldCache(false)
        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy
        MySingleton.getInstance(context).addToRequestQueue(stringRequest)
    }

    private fun setspinnerdata_onui(it: String?) {
        try {

            val list: MutableList<String> = ArrayList()
          val listbool: MutableList<Boolean> = ArrayList()

            var obj= JSONArray(it)

            for (i in 0..obj.length()-1){
                if (obj.getString(i).equals("GT")){
                    list.add(0,obj.getString(i))
                }else{
                    list.add(obj.getString(i))
                }
                listbool.add(false)
            }
            listbool.set(0,true)

//            val dataAdapter= ArrayAdapter<String>(
//                    activity!!,
//                    android.R.layout.simple_spinner_item, list
//            )
//            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            view1. recyclerviewhorizontal.layoutManager =LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
            view1. recyclerviewhorizontal.setAdapter(Category_Horizontal_Adapter(activity!!,list,listbool,this))

//            view1. spinnder.setAdapter(dataAdapter)
//            view1. spinnder.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
//                override fun onNothingSelected(parent: AdapterView<*>?) {
//
//                }
//
//                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//                    spinnervalue = list.get(position)
//                    loggg("call dropdown","drop")
////                    getdatacategory("01/03/2020","31/03/2020",activity!!)
//                    getdatacategory(Common.one_month(strcaldate).toString(),strcaldate,activity!!)
//                }
//
//            }
            if (list.size>0){
                spinnervalue = list.get(0)

            }
            if (Paper.book(TradeBook).read<String>(TradeJson)!=null){
                setdataonuigraph(Paper.book(TradeBook).read<String>(TradeJson),
                    Common.one_month(strcaldate).toString(),strcaldate, activity!!,"paper")
            }else {
                getdata(Common.one_month(strcaldate).toString(),strcaldate,activity!!)
//                getdata("01/03/2020", "31/03/2020", activity!!)
            }
        }catch (e: Exception) {
            Log.e("error","",e);
            progressDialog.cancel()
        }
    }


    override fun tabclick(str: String) {
        spinnervalue = str
        loggg("call dropdown","drop")
//                    getdatacategory("01/03/2020","31/03/2020",activity!!)
        getdatacategory(Common.one_month(strcaldate).toString(),strcaldate,activity!!)
    }
    private fun getdatacategory(fromdate:String,todate:String,context: Context) {

//        val pd= DelayedProgressDialog()
//        pd.show(fragmentManager!!,"")
        val url = baseurl_managment+"GetCategories?from_date=$fromdate&to_date=$todate&trade_name=$spinnervalue"
        val stringRequest: StringRequest = object:StringRequest(Method.GET,url,
                Response.Listener {
                    Common.loggg("gethome", it)
                    Common.loggg("url", url)
                    Paper.book(CategoryTableBook).write(CategoryTableJson,it)
                    setup_tabledata_ui(it)
                    progressDialog.cancel()

                },
                Response.ErrorListener {
                    progressDialog.cancel()
                    Common.toast(context, Common.volleyerror(it))
                })
        {
            override  fun getParams(): Map<String, String>  {
                val params2: MutableMap<String, String> = HashMap()

//                params2["name"]= "${personName}"
                return params2
            }
            override fun getHeaders(): MutableMap<String, String> {
                val params2: MutableMap<String, String> = HashMap()
                params2.put("Authorization","Bearer ${UserSharedPrefrences.getInstance(context).gettoken()}")
                return params2
            }
        }
        stringRequest.setShouldCache(false)
        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy
        MySingleton.getInstance(context).addToRequestQueue(stringRequest)
    }

    private fun setup_tabledata_ui(it: String?) {
        list.clear()
        try {


            var obj= JSONObject(it)
            if(obj.getString("status").equals("true")) {
                val jsonObject = obj.getJSONArray("data")
//                        total = jsonObject.getLong("total_Sales")
                total = 10
                Common.loggg("bool1", total.toString())
                if (total > 0){


//                category
                    if (jsonObject.length()>0){
                        var tradelong :Double= 0.0
                        for (i in 0..jsonObject.length() - 1) {
                            val jsonObjecttrade = jsonObject.getJSONObject(i)

                            list.add(
                                    Category(
                                            Common.TxtFormate(jsonObjecttrade.getString("category_Name")),
                                            Common.TxtFormate( jsonObjecttrade.getString("product_Name")), roundofstring(jsonObjecttrade.getString("total_Sale_Amount")),
                                            roundofstring(   jsonObjecttrade.getString("mtd_Amount")), jsonObjecttrade.getString("lmtd_Amount")
                                            ,jsonObjecttrade.getString("growth_Percentage"))
                            )
                        }
//                        view1.horizontabscrol.visibility = View.VISIBLE
                        view1.recycler.adapter =
                                Category_wise_Adapter(
                                        activity,
                                        list
                                )
                    }else{
//                        view1.horizontabscrol.visibility = View.GONE
                    }


                    view1.scrollView.visibility = View.VISIBLE
                    view1.rl_empty.visibility = View.GONE

                }else{

                    view1.scrollView.visibility = View.GONE
                    view1.rl_empty.visibility = View.VISIBLE
                    view1.tv_totalsales.setText("₹ 0")
                }

            }else{
                view1.scrollView.visibility = View.GONE
                view1.rl_empty.visibility = View.VISIBLE
                view1.tv_totalsales.setText("₹ 0")
            }

        }catch (e: Exception) {
            progressDialog.cancel()
            Log.e("error","",e);
        }
    }


    fun shocalender(){
        val transition: Transition = Slide(Gravity.RIGHT)
        transition.setDuration(600)
        transition.addTarget(view1.view_calender)
        TransitionManager.beginDelayedTransition(view1.rlparant, transition);
        view1.view_calender.visibility = View.VISIBLE

    }
    fun hidecalender(){
        val transition: Transition = Slide(Gravity.RIGHT)
        transition.setDuration(600)
        transition.addTarget(view1.view_calender)
        TransitionManager.beginDelayedTransition(view1.rlparant, transition);
        view1. view_calender.visibility = View.GONE


    }



    fun changecolor(textView: TextView, textView1: TextView, textView2: TextView){
        textView.setTextColor(ContextCompat.getColor(activity!!,R.color.white))
        textView.background = ContextCompat.getDrawable(activity!!,R.drawable.selected_tab_bg)

        textView1.setTextColor(ContextCompat.getColor(activity!!,R.color.bottomsheet_blue))
        textView1.background = null

        textView2.setTextColor(ContextCompat.getColor(activity!!,R.color.bottomsheet_blue))
        textView2.background = null



    }



    var total :Long = 0
    private fun getdata(fromdate:String,todate:String,context: Context) {
        total =0
//        entries1.clear()
//        progressDialog.cancel()
//        progressDialog.show(fragmentManager!!,"")
//        val pd= DelayedProgressDialog()
//        pd.show(fragmentManager!!,"")
        val url = baseurl_managment+"gethome?from_date=$fromdate&to_date=$todate"
//       val url = baseurl_managment+"gethome?from_date=01/03/2020&to_date=31/03/2020"
        val stringRequest: StringRequest = object:StringRequest(Method.GET,url,
                Response.Listener {
                    Common.loggg("gethome", it)
                    Common.loggg("url", url)
                    setdataonuigraph(it,fromdate, todate, context,"apicall")

                },
                Response.ErrorListener {
                    progressDialog.cancel()
                    Common.toast(context, Common.volleyerror(it))
                })
        {
            override  fun getParams(): Map<String, String>  {
                val params2: MutableMap<String, String> = HashMap()

//                params2["name"]= "${personName}"
                return params2
            }
            override fun getHeaders(): MutableMap<String, String> {
                val params2: MutableMap<String, String> = HashMap()
                params2.put("Authorization","Bearer ${UserSharedPrefrences.getInstance(context).gettoken()}")
                return params2
            }
        }
        stringRequest.setShouldCache(false)
        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy
        MySingleton.getInstance(context).addToRequestQueue(stringRequest)
    }

    var totalsales_category :Long = 0
    var list1= ArrayList<TradeTotal>()

    fun  setdataonuigraph(it: String,fromdate: String,todate: String,context: Context,calltype:String){
        list1.clear()
        totalsales_category=0
        try {


            var obj= JSONObject(it)
            if(obj.getString("status").equals("true")) {
                val jsonObject = obj.getJSONObject("data")
                total = jsonObject.getLong("total_Sales")
                Common.loggg("bool1", total.toString())
                Common.loggg("bool", (total > 0).toString() + "")
                if (total > 0){
                    view1.tv_totalsales.setText("₹ "+getFormatedNumber(jsonObject.getLong("total_Sales").toString()))


//                category
                    if (jsonObject.getJSONArray("categories").length()>0) {
                        var categorylong :Double= 0.0
                        for (i in 0..jsonObject.getJSONArray("categories").length() - 1) {
                            val jsonObjecttrade =
                                jsonObject.getJSONArray("categories").getJSONObject(i)
                            totalsales_category = totalsales_category+jsonObjecttrade.getLong("total_Sale_Amount").toLong()
                        }
                    }

                    if (jsonObject.getJSONArray("categories").length()>0) {
                        var categorylong :Double= 0.0
                        for (i in 0..jsonObject.getJSONArray("categories").length() - 1) {
                            val jsonObjecttrade =
                                    jsonObject.getJSONArray("categories").getJSONObject(i)


                            val  roundvalue = roundof((jsonObjecttrade.getLong("total_Sale_Amount").toFloat()/ totalsales_category * 100).toString())
                            if (roundvalue != null) {
                                Common.loggg(
                                    "categoriesprecentage",
                                    (jsonObjecttrade.getLong("total_Sale_Amount")
                                        .toFloat() / totalsales_category * 100).toString()
                                            + " - " + jsonObjecttrade.getString("category_Name")
                                            + " - " + (roundvalue > (0.1)).toString() + " - " + roundvalue
                                )
                            }

                            list1.add(TradeTotal(jsonObjecttrade.getString("category_Name"),jsonObjecttrade.getString("total_Sale_Amount")))

                            if (roundvalue != null) {
                                if (roundvalue>=(10)) {
//                                    entries1.add(
//                                        PieEntry(
//                                            roundvalue.toFloat(),
//                                            Common.TxtFormate(jsonObjecttrade.getString("category_Name"))
//                                        )
//                                    )

                                }else{

                                    categorylong +=roundvalue

                                }
                            }
                        }
                        if (categorylong!=0.0){
//                            entries1.add(
//                                    PieEntry(
//                                            categorylong.toFloat()
//                                            , "Other"
//                                    )
//                            )
                        }
//                        llregion
//                        view1.chart1.visibility = View.VISIBLE
                    }

//                    addchartdata(view1.chart1, entries1)


                    view1.recyclercat.layoutManager = LinearLayoutManager(activity)
                    view1.recyclercat.adapter = Total_Category_Wise_Adapter(activity,list1)

                    view1.rl_empty.visibility = View.GONE
                    view1.scrollView.visibility = View.VISIBLE


                }else{
                    view1.scrollView.visibility = View.GONE
                    view1.rl_empty.visibility = View.VISIBLE
                    view1.tv_totalsales.setText("₹ 0")
                }

            }else{
                view1.scrollView.visibility = View.GONE
                view1.rl_empty.visibility = View.VISIBLE
                view1.tv_totalsales.setText("₹ 0")
            }


            if (calltype.equals("apicall")){
                getdatacategory(fromdate, todate, context)
            }else{
                if (Paper.book(CategoryTableBook).read<String>(CategoryTableJson)!=null){
              setup_tabledata_ui(Paper.book(CategoryTableBook).read<String>(CategoryTableJson))
            }
            progressDialog.cancel()
            }

        }catch (e: Exception) {
            Log.e("error","",e);
            progressDialog.cancel()
        }

        getgetcoearthdata(fromdate,todate,context)
    }


    fun seven_day(string: String): String?{
        var df =
                SimpleDateFormat("dd/MM/yyyy", Locale.US)
        val c = Calendar.getInstance()
        val myDate: Date = df.parse(string)
        c.time = myDate
        c.add(Calendar.DAY_OF_YEAR, -6);
        val newDate = c.getTime()
        var date = df.format(newDate);
        loggg("7date",date)
        loggg("71date", Common.dateof(string))
        loggg("72date", Common.dateafterof(string))

        if (Common.dateof(string).toInt()>6){
            date = date
            view1.tv_weekdate.text = "${Common.datelastof(date)} - ${current_date_formated(1)}"
        }else{

            date = "01${Common.dateafterof(string)}"
            view1.tv_weekdate.text = "01${Common.dateafterof(current_date_formated(1)!!)} - ${current_date_formated(1)}"
        }
        return  date
    }


    fun sendcsv(){
        val csv: String =activity!!.getExternalFilesDir(null)!!.getAbsolutePath().toString() + "/Category_wise.csv" // Here csv file name is MyCsvFile.csv

        var writer: CSVWriter? = null
        try {
            writer = CSVWriter(FileWriter(csv))
            val data: MutableList<Array<String>> =
                ArrayList()
            data.add(arrayOf("Category Name", "Product Name", "Net Sale", "MTD", "LMTD", "Groth (%)"))
            for (i in 0..list.size- 1) {
                data.add(arrayOf(list.get(i).categoryname,list.get(i).producname,list.get(i).netsales,list.get(i).mtd,
                    list.get(i).lymtd,list.get(i).growth))


            }



            writer.writeAll(data) // data is adding to csv
            writer.close()

        } catch (e: IOException) {
            e.printStackTrace()
        }

        val emailIntent = Intent(Intent.ACTION_SEND)
        emailIntent.type = "text/plain"

        val file = File(csv)
        val uri: Uri =   FileProvider.getUriForFile(
            activity!!,
            BuildConfig.APPLICATION_ID, //(use your app signature + ".provider" )
            file);
//        val uri: Uri = Uri.fromFile(file)
        emailIntent.putExtra(Intent.EXTRA_STREAM, uri)

        startActivity(Intent.createChooser(emailIntent, null))
    }


    private fun getgetcoearthdata(fromdate: String, todate: String, context: Context) {


        val pd = DelayedProgressDialog()
        pd.show(fragmentManager!!, "")
        val url = baseurl_managment + "getcoearth?from_date=$fromdate&to_date=$todate"
//       val url = baseurl_managment+"gethome?from_date=01/03/2020&to_date=31/03/2020"
        val stringRequest: StringRequest = object : StringRequest(
            Method.GET, url,
            Response.Listener {
                Common.loggg("gethome", it)
                Common.loggg("url", url)
                var obj= JSONObject(it)
                if(obj.getString("status").equals("true")) {
                    val jsonObject = obj.getJSONObject("data")

                    var totalwithglob=total+jsonObject.getLong("total_Sales")
                    loggg("inputvalue","$totalwithglob")
                    if (totalwithglob.toString().contains("E")){
                        totalwithglob = totalwithglob.toString().replace("E","").toLong()
                    }
                    view1.tv_totalsales.setText("₹ "+getFormatedNumber(totalwithglob.toString()))

//                category
                if (jsonObject.getJSONArray("categories").length()>0) {
                    var categorylong :Double= 0.0
                    for (i in 0..jsonObject.getJSONArray("categories").length() - 1) {
                        val jsonObjecttrade =
                            jsonObject.getJSONArray("categories").getJSONObject(i)
                        totalsales_category = totalsales_category+jsonObjecttrade.getLong("total_Sale_Amount").toLong()
                    }
                }

                if (jsonObject.getJSONArray("categories").length()>0) {
                    var categorylong :Double= 0.0
                    for (i in 0..jsonObject.getJSONArray("categories").length() - 1) {
                        val jsonObjecttrade =
                            jsonObject.getJSONArray("categories").getJSONObject(i)


                        val  roundvalue = roundof((jsonObjecttrade.getLong("total_Sale_Amount").toFloat()/ totalsales_category * 100).toString())
                        if (roundvalue != null) {
                            Common.loggg(
                                "categoriesprecentage",
                                (jsonObjecttrade.getLong("total_Sale_Amount")
                                    .toFloat() / totalsales_category * 100).toString()
                                        + " - " + jsonObjecttrade.getString("category_Name")
                                        + " - " + (roundvalue > (0.1)).toString() + " - " + roundvalue
                            )
                        }

                        list1.add(TradeTotal(jsonObjecttrade.getString("category_Name"),jsonObjecttrade.getString("total_Sale_Amount")))

                        if (roundvalue != null) {
                            if (roundvalue>=(10)) {
//                                    entries1.add(
//                                        PieEntry(
//                                            roundvalue.toFloat(),
//                                            Common.TxtFormate(jsonObjecttrade.getString("category_Name"))
//                                        )
//                                    )

                            }else{

                                categorylong +=roundvalue

                            }
                        }
                    }
                    if (categorylong!=0.0){
//                            entries1.add(
//                                    PieEntry(
//                                            categorylong.toFloat()
//                                            , "Other"
//                                    )
//                            )
                    }
//                        llregion
//                        view1.chart1.visibility = View.VISIBLE
                }

//                    addchartdata(view1.chart1, entries1)


                view1.recyclercat.layoutManager = LinearLayoutManager(activity)
                view1.recyclercat.adapter = Total_Category_Wise_Adapter(activity,list1)
                }
                pd.cancel()

            },
            Response.ErrorListener {
                pd.cancel()
                Common.toast(context, Common.volleyerror(it))
            }) {
            override fun getParams(): Map<String, String> {
                val params2: MutableMap<String, String> = java.util.HashMap()

//                params2["name"]= "${personName}"
                return params2
            }

            override fun getHeaders(): MutableMap<String, String> {
                val params2: MutableMap<String, String> = java.util.HashMap()
                params2.put(
                    "Authorization",
                    "Bearer ${UserSharedPrefrences.getInstance(context).gettoken()}"
                )
                return params2
            }
        }
        stringRequest.setShouldCache(false)
        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        stringRequest.retryPolicy = policy
        MySingleton.getInstance(context).addToRequestQueue(stringRequest)
    }


}