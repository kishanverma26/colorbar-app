package com.android.colorbarapp.Fragment.EBO

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.PopupMenu
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.android.colorbarapp.Adapter.Monthly_Adapter
import com.android.colorbarapp.CustomClasses.DelayedProgressDialog
import com.android.colorbarapp.CustomClasses.MySingleton
import com.android.colorbarapp.CustomClasses.SpacesItemDecoration
import com.android.colorbarapp.Login_Activity
import com.android.colorbarapp.Model.Monthly_Order
import com.android.colorbarapp.R
import com.android.colorbarapp.Utils.ParamKeys
import com.android.colorbarapp.Utils.UserSharedPrefrences
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.request.StringRequest
import com.s.strokeclock.Utils.Common
import com.s.strokeclock.Utils.Common.Companion.getFormatedNumber
import io.paperdb.Paper
import kotlinx.android.synthetic.main.header_topbar.*
import kotlinx.android.synthetic.main.header_topbar.view.*
import kotlinx.android.synthetic.main.monthly_fragmnet.view.*
import org.json.JSONObject

class Monthly_Order_Fragment : Fragment() , ParamKeys {

    lateinit var mcoxt: Context
    lateinit var view1: View
    lateinit var list: ArrayList<Monthly_Order>
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.monthly_fragmnet,container,false)
        mcoxt= activity!!
        view1 = view
        initi();
        return view
    }

    private fun initi() {
        list = ArrayList()


        val spacingInPixels =
            resources.getDimensionPixelSize(R.dimen.d_20dp)
        view1.recycler.layoutManager = GridLayoutManager(activity,2)
        view1.recycler.addItemDecoration(SpacesItemDecoration(2, spacingInPixels, true))

        view1.recycler.adapter = Monthly_Adapter(activity,list)
        view1.tvusername.setText(UserSharedPrefrences.getInstance(activity!!).getname())
        view1. iv_icon.setOnClickListener { v -> Common.popup_Menu(activity!!,activity!!,view1. iv_icon) }


        if (Paper.book(MonthlyBook).read<String>(Monthlyres)!=null){
            setdataonui(Paper.book(MonthlyBook).read<String>(Monthlyres))
        }

        getdata(activity!!)
    }

    private fun getdata(context: Context) {


        val pd= DelayedProgressDialog()
        pd.show(fragmentManager!!,"")
        val url = baseurl_ebo+"GetMonthlyTargetReport"

        val stringRequest: StringRequest = object: StringRequest(Method.POST,url,
                Response.Listener {
                    Common.loggg("GetMonthlyTargetReport", it)
                    Common.loggg("url", url)
                    Paper.book(MonthlyBook).write(Monthlyres,it)

                    setdataonui(it)
                    pd.cancel()

                },
                Response.ErrorListener {
                    pd.cancel()
                    Common.toast(context, Common.volleyerror(it))
                })
        {
            override  fun getParams(): Map<String, String>  {
                val params2: MutableMap<String, String> = HashMap()

//                params2["name"]= "${personName}"
                return params2
            }

            override fun getHeaders(): MutableMap<String, String> {
                val params2: MutableMap<String, String> = HashMap()
                params2.put("Authorization","Bearer ${UserSharedPrefrences.getInstance(context).gettoken()}")
                return params2
            }

        }
        stringRequest.setShouldCache(false)
        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy
        MySingleton.getInstance(context).addToRequestQueue(stringRequest)
    }

    private fun setdataonui(it: String?) {
        list.clear()

        try {


            var obj1= JSONObject(it)
            if(obj1.getString("status").equals("true")) {
                var obj =obj1.getJSONObject("data")
//                Product data


                list.add(Monthly_Order("No. of \nstores",getFormatedNumber(obj.getString("no_Of_Stores"))!!))
                list.add(Monthly_Order("Target",getFormatedNumber(obj.getString("target"))!!))
                list.add(Monthly_Order("FTD",Common.roundof(Common.get_valuinlaks(obj.getString("ftm_Amount"))!!).toString()))
                list.add(Monthly_Order("MTD",Common.roundof(Common.get_valuinlaks(obj.getString("mtd_Amount"))!!).toString()))
                list.add(Monthly_Order("LYMTD",getFormatedNumber(obj.getString("lymtd_Amount"))!!))
                list.add(Monthly_Order("Achievement",obj.getString("achievement_Percentage")+" %"))
                list.add(Monthly_Order("% Growth on \nLY MTD",obj.getString("growth_Percentage_Lymtd")+" %"))


                view1.recycler.adapter!!.notifyDataSetChanged()

            }
        }catch (e: Exception) {
            Log.e("error","",e);
        }
    }
}