package com.android.colorbarapp.Model

class Category(categoryname:String,producname:String, netsales:String, mtd:String, lymtd:String, growth:String) {

    var categoryname: String = categoryname
        get() = field
        set(value) {
            field = value
        }
   var producname: String = producname
        get() = field
        set(value) {
            field = value
        }

     var netsales: String = netsales
        get() = field
        set(value) {
            field = value
        }

  var mtd: String = mtd
        get() = field
        set(value) {
            field = value
        }

     var lymtd: String = lymtd
        get() = field
        set(value) {
            field = value
        }

    var growth: String = growth
        get() = field
        set(value) {
            field = value
        }


}