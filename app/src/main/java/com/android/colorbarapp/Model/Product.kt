package com.android.colorbarapp.Model

class Product(pname:String, categoryname:String,sold_quantity:String, revenue:String) {

    var pname: String = pname
        get() = field
        set(value) {
            field = value
        }

     var categoryname: String = categoryname
        get() = field
        set(value) {
            field = value
        }

  var sold_quantity: String = sold_quantity
        get() = field
        set(value) {
            field = value
        }

     var revenue: String = revenue
        get() = field
        set(value) {
            field = value
        }


}