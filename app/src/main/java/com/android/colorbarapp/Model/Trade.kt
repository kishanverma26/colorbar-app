package com.android.colorbarapp.Model

class Trade(
    trdename: String? = null,
    netsales: String? = null,
    mtd: String? = null,
    lymtd: String? = null,
    growth: String? = null
) {

    var trdename: String? = trdename
        get() = field
        set(value) {
            field = value
        }

    var netsales: String? = netsales
        get() = field
        set(value) {
            field = value
        }

    var mtd: String? = mtd
        get() = field
        set(value) {
            field = value
        }

    var lymtd: String? = lymtd
        get() = field
        set(value) {
            field = value
        }

    var growth: String? = growth
        get() = field
        set(value) {
            field = value
        }


}