package com.android.colorbarapp.Model

class ExpendableTrade(trdename:String?=null, netsales:String?=null, mtd:String?=null, lymtd:String?=null, growth:String?=null,
                      isselected:Boolean?=false,subcatlist:ArrayList<Trade>?=null) {

    var trdename: String? = trdename
        get() = field
        set(value) {
            field = value
        }

     var netsales: String? = netsales
        get() = field
        set(value) {
            field = value
        }

  var mtd: String? = mtd
        get() = field
        set(value) {
            field = value
        }

     var lymtd: String? = lymtd
        get() = field
        set(value) {
            field = value
        }

    var growth: String? = growth
        get() = field
        set(value) {
            field = value
        }

    var tradelist: ArrayList<Trade>? = subcatlist
        get() = field
        set(value) {
            field = value
        }
    var indexPos: Int = 100
        get() = field
        set(value) {
            field = value
        }

   var isselected: Boolean? = isselected
        get() = field
        set(value) {
            field = value
        }

}