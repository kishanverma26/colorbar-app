package com.android.colorbarapp.Model

class Gross(trdename:String, gross_Amount:String, return_Amount:String,netsales:String) {

    var trdename: String = trdename
        get() = field
        set(value) {
            field = value
        }

     var gross_Amount: String = gross_Amount
        get() = field
        set(value) {
            field = value
        }

  var return_Amount: String = return_Amount
        get() = field
        set(value) {
            field = value
        }

 var netsales: String = netsales
        get() = field
        set(value) {
            field = value
        }



}