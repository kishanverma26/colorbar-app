package com.android.colorbarapp

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.colorbarapp.Adapter.EBO.Product_Adapter
import com.android.colorbarapp.Model.Product
import com.s.strokeclock.Utils.Common
import kotlinx.android.synthetic.main.category_wise_detail.*

class Group_wise_Detail_Activity : AppCompatActivity() {

    lateinit var mcoxt: Context
    lateinit var view1: View
    lateinit var list: ArrayList<Product>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Common.hideSoftKeyboard(this)
        setContentView(R.layout.category_wise_detail)
        initi()

    }


    private fun initi() {
        list = ArrayList()
        list.add(Product("Liquid Matte","Lipstick","45","9,068"))
        list.add(Product("Glossy","Lipstick","56","18,500"))
      list.add(Product("Kajal","Eye Care","56","42,711"))
   list.add(Product("Mascara","Eye Care","24","38,820"))

         list.add(Product("Liquid Matte","Lipstick","45","9,068"))
        list.add(Product("Glossy","Lipstick","56","18,500"))
      list.add(Product("Kajal","Eye Care","56","42,711"))
   list.add(Product("Mascara","Eye Care","24","38,820"))

         list.add(Product("Liquid Matte","Lipstick","45","9,068"))
        list.add(Product("Glossy","Lipstick","56","18,500"))
      list.add(Product("Kajal","Eye Care","56","42,711"))
   list.add(Product("Mascara","Eye Care","24","38,820"))

         list.add(Product("Liquid Matte","Lipstick","45","9,068"))
        list.add(Product("Glossy","Lipstick","56","18,500"))
      list.add(Product("Kajal","Eye Care","56","42,711"))
   list.add(Product("Mascara","Eye Care","24","38,820"))

        recycler.layoutManager = LinearLayoutManager(this)
        recycler.adapter = Product_Adapter(this, list)

        Common.hideSoftKeyboard(this)
    }



}