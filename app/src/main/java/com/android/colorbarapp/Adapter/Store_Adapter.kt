package com.android.colorbarapp.Adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.colorbarapp.Model.Monthly_Order
import com.android.colorbarapp.Model.Store
import com.android.colorbarapp.R
import com.s.strokeclock.Utils.Common
import com.s.strokeclock.Utils.Common.Companion.getFormatedNumber
import kotlinx.android.synthetic.main.store_report_item.view.*


class Store_Adapter(val context: Context?,val list: ArrayList<Store>) : RecyclerView.Adapter<Store_Adapter.ViewHolder>() {



    class ViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView) {

        val tvmonthly=itemView.tvmonthly
        val tv_daily=itemView.tv_daily
        val tv_store=itemView.tv_store

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.store_report_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener { v ->
//            context!!.startActivity(Intent(context,Order_Detail_Activity::class.java))
        }

        holder.tvmonthly.text = Common.roundof(Common.get_valuinlaks(list.get(position).monthly)!!).toString()
                    holder . tv_daily . text = Common.roundof(
                        Common.get_valuinlaks(list.get(position).daily)!!).toString()
                                holder . tv_store . text = list . get (position).storename

    }

    override fun getItemCount(): Int {
        return list.size
    }

}