package com.android.colorbarapp.Adapter.EBO

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.colorbarapp.Model.Product
import com.android.colorbarapp.R
import com.s.strokeclock.Utils.Common
import com.s.strokeclock.Utils.Common.Companion.getFormatedNumber
import kotlinx.android.synthetic.main.product_report_item.view.*


class Product_Adapter(val context: Context?,val  list: ArrayList<Product>) : RecyclerView.Adapter<Product_Adapter.ViewHolder>() {



    class ViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView) {

        val tv_productname=itemView.tv_productname
        val tv_category=itemView.tv_category
        val tv_sold=itemView.tv_sold
        val tv_revenu=itemView.tv_revenu
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.product_report_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener { v ->
//            context!!.startActivity(Intent(context,Order_Detail_Activity::class.java))
        }

        holder.tv_productname.text = list.get(position).pname
        holder.tv_category.text = list.get(position).categoryname
        holder.tv_sold.text = list.get(position).sold_quantity
//        holder.tv_revenu.text = String.format("%,d",list.get(position).revenue.toLong())
    holder.tv_revenu.text = Common.roundof(Common.get_valuinlaks(list.get(position).revenue)!!).toString()

    }

    override fun getItemCount(): Int {
        return list.size
    }

}