package com.android.colorbarapp.Adapter.EBO

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.colorbarapp.Model.Monthly_Order
import com.android.colorbarapp.R
import com.s.strokeclock.Utils.Common
import com.s.strokeclock.Utils.Common.Companion.getFormatedNumber
import kotlinx.android.synthetic.main.consolidate_item.view.*


class Revenue_Adapter(val context: Context?, val list: ArrayList<Monthly_Order>) : RecyclerView.Adapter<Revenue_Adapter.ViewHolder>() {



    class ViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView) {

        val tvtital=itemView.tvtital
        val tvquantity=itemView.tvquantity
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.consolidate_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener { v ->
//            context!!.startActivity(Intent(context,Order_Detail_Activity::class.java))
        }
        holder.tvtital.text = list.get(position).tital
        holder.tvquantity.text = Common.roundof(
            Common.get_valuinlaks(list.get(position).quantity)!!).toString()
        Log.e( "quantity: ",list.get(position).quantity+"" )


    }

    override fun getItemCount(): Int {
        return list.size
    }

}