package com.android.colorbarapp.Adapter.Managment

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.colorbarapp.Model.ExpendableTrade
import com.android.colorbarapp.Model.Trade
import com.android.colorbarapp.R
import com.s.strokeclock.Utils.Common.Companion.get_valuinlaks
import com.s.strokeclock.Utils.Common.Companion.loggg
import com.s.strokeclock.Utils.Common.Companion.roundof
import kotlinx.android.synthetic.main.expendable_trade_report_item.view.*


class Expendable_Trade_Wise_Adapter(val context: Context?, val list: ArrayList<ExpendableTrade>
) :
    RecyclerView.Adapter<Expendable_Trade_Wise_Adapter.ViewHolder>() {


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val tv_tradename = itemView.tv_tradename
        val recyclerview: RecyclerView = itemView.recyclerview
        val tv_netsales = itemView.tv_netsales
        val tv_mtd = itemView.tv_mtd
        val tv_groth = itemView.tv_groth
        val tv_lymtd = itemView.tv_lymtd

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.expendable_trade_report_item, parent, false)
        return ViewHolder(
            view
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (list.get(position).tradelist==null){
            holder.tv_tradename.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
        }else {
            if (list.get(position).tradelist?.size!! > 0) {
                holder.tv_tradename.setCompoundDrawablesWithIntrinsicBounds(
                    null, null, ContextCompat.getDrawable(
                        context!!, R.drawable.ic_right_arrow
                    ), null
                )
            // setting recycler view
                holder.recyclerview.layoutManager = LinearLayoutManager(context)
                holder.recyclerview.adapter = Trade_Wise_Adapter(context,
                    list[position].tradelist!!
                )
                holder.itemView.setOnClickListener { v ->
                    if (list[position].isselected == true){
                        list[position].isselected= false
                        holder.recyclerview.visibility = View.GONE
                        holder.tv_tradename.setCompoundDrawablesWithIntrinsicBounds(
                            null, null, ContextCompat.getDrawable(
                                context!!, R.drawable.ic_right_arrow
                            ), null
                        )
                    }else{
                        list[position].isselected= true
                        holder.recyclerview.visibility = View.VISIBLE
                        holder.tv_tradename.setCompoundDrawablesWithIntrinsicBounds(
                            null, null, ContextCompat.getDrawable(
                                context!!, R.drawable.ic_down_arrow
                            ), null
                        )
                    }
                }


            } else {
                holder.tv_tradename.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
            }
        }


        holder.tv_tradename.text = list.get(position).trdename
        if (list.get(position).netsales.equals("null") || list.get(position).netsales == null) {
            holder.tv_netsales.text = "0"
        } else {

            holder.tv_netsales.text =
                roundof(get_valuinlaks(list.get(position).netsales!!)!!).toString()
        }


        holder.tv_mtd.text = roundof(get_valuinlaks(list.get(position).mtd!!)!!).toString()


        holder.tv_lymtd.text = roundof(get_valuinlaks(list.get(position).lymtd!!)!!).toString()
//        loggg("growth0",list.get(position).growth+"")
        try {

            holder.tv_groth.text = list.get(position).growth

        } catch (exe: NumberFormatException) {
            loggg("error", "exe", exe)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

}