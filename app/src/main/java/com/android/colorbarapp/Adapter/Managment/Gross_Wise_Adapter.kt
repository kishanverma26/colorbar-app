package com.android.colorbarapp.Adapter.Managment

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.colorbarapp.Model.Gross
import com.android.colorbarapp.R
import com.s.strokeclock.Utils.Common
import com.s.strokeclock.Utils.Common.Companion.getFormatedNumber
import kotlinx.android.synthetic.main.trade_gross_item.view.*


class Gross_Wise_Adapter(val context: Context?, val  list: ArrayList<Gross>) : RecyclerView.Adapter<Gross_Wise_Adapter.ViewHolder>() {



    class ViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView) {

        val tv_tradename=itemView.tv_tradename
        val tv_netsales=itemView.tv_netsales
        val tv_mtd=itemView.tv_mtd
        val tv_groth=itemView.tv_lymtd
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.trade_gross_item, parent, false)
        return ViewHolder(
            view
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener { v ->
//            context!!.startActivity(Intent(context,Order_Detail_Activity::class.java))
        }

        holder.tv_tradename.text = list.get(position).trdename
        holder.tv_netsales.text = Common.roundof(
            Common.get_valuinlaks(list.get(position).gross_Amount)!!).toString()
                    holder . tv_mtd . text = Common.roundof(
                        Common.get_valuinlaks(list.get(position).return_Amount)!!).toString()
                                holder . tv_groth . text =
                                    Common.roundof(Common.get_valuinlaks(list.get(position).netsales)!!).toString()


//        try {
//
//
//        if (list.get(position).growth.toDouble()>0){
//        holder.tv_groth.text = list.get(position).growth
//    }else{
//            holder.tv_groth.text = "Nil"
//        }
//        }catch ( exe :NumberFormatException){
//            loggg("error","exe",exe)
//        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

}