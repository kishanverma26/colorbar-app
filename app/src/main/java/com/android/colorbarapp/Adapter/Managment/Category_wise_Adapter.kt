package com.android.colorbarapp.Adapter.Managment

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.colorbarapp.Model.Category
import com.android.colorbarapp.R
import com.s.strokeclock.Utils.Common
import com.s.strokeclock.Utils.Common.Companion.loggg
import kotlinx.android.synthetic.main.category_wise_item.view.*


class Category_wise_Adapter(val context: Context?, val list: ArrayList<Category>) :
    RecyclerView.Adapter<Category_wise_Adapter.ViewHolder>() {


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val tv_product = itemView.tv_product
        val tv_netsales = itemView.tv_netsales
        val tv_mtd = itemView.tv_mtd
        val tv_groth = itemView.tv_groth
        val tv_lymtd = itemView.tv_lymtd
        val tv_catname = itemView.tv_catname
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.category_wise_item, parent, false)
        return ViewHolder(
            view
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener { v ->
//            context!!.startActivity(Intent(context,Order_Detail_Activity::class.java))
        }
        holder.tv_catname.text = list.get(position).categoryname
        holder.tv_product.text = list.get(position).producname
//        loggg("number",list.get(position).netsales)
        holder.tv_netsales.text = Common.roundof(
            Common.get_valuinlaks(list.get(position).netsales)!!).toString()
                    holder . tv_mtd . text = Common.roundof(
                        Common.get_valuinlaks(list.get(position).mtd)!!).toString()
                        try {

                             holder.tv_groth.text = list.get(position).growth

                        } catch (exe: NumberFormatException) {
                            loggg("error", "exe", exe)
                        }
                                holder . tv_lymtd . text =
                                    Common.roundof(Common.get_valuinlaks(list.get(position).lymtd)!!).toString()

    }

    override fun getItemCount(): Int {
        return list.size
    }

}