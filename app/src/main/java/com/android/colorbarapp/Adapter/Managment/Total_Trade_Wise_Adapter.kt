package com.android.colorbarapp.Adapter.Managment

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.colorbarapp.Model.Trade
import com.android.colorbarapp.R
import com.s.strokeclock.Utils.Common
import com.s.strokeclock.Utils.Common.Companion.getFormatedNumber
import kotlinx.android.synthetic.main.total_trade_report_item.view.*


class Total_Trade_Wise_Adapter(val context: Context?, val  list: ArrayList<Trade>) : RecyclerView.Adapter<Total_Trade_Wise_Adapter.ViewHolder>() {



    class ViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView) {

        val tv_tradename=itemView.tv_tradename

        val tv_groth=itemView.tv_groth

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.total_trade_report_item, parent, false)
        return ViewHolder(
            view
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener { v ->
//            context!!.startActivity(Intent(context,Order_Detail_Activity::class.java))
        }

        holder.tv_tradename.text = list.get(position).trdename
        holder.tv_groth.text = Common.roundof(Common.get_valuinlaks(list.get(position).netsales!!)!!).toString()



    }

    override fun getItemCount(): Int {
        return list.size
    }

}