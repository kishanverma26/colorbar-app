package com.android.colorbarapp.Adapter.Managment

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.colorbarapp.Fragment.Managment.Category_Wise_Fragment
import com.android.colorbarapp.Interfaces.TabClick
import com.android.colorbarapp.Model.Trade
import com.android.colorbarapp.R
import com.s.strokeclock.Utils.Common.Companion.TxtFormate
import com.s.strokeclock.Utils.Common.Companion.getFormatedNumber
import kotlinx.android.synthetic.main.category_horizontal_adapter.view.*


class Category_Horizontal_Adapter(val context: Context?, val list: MutableList<String>,val listbool: MutableList<Boolean>,val call : TabClick) : RecyclerView.Adapter<Category_Horizontal_Adapter.ViewHolder>() {



    class ViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView) {

        val tv_tital=itemView.tv_tital

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.category_horizontal_adapter, parent, false)
        return ViewHolder(
            view
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener { v ->
//            context!!.startActivity(Intent(context,Order_Detail_Activity::class.java))
            for (i in 0..listbool.size-1){
                if (listbool.get(i)){
                    listbool.set(i,false)
                }

                listbool.set(position,true)
            }
            call.tabclick(list.get(position))
            notifyDataSetChanged()


        }

        holder.tv_tital.text =TxtFormate(list.get(position))

        if (listbool.get(position)){
            holder.tv_tital.setBackgroundResource(R.drawable.selected_tab_bg_golden)

        }else{
            holder.tv_tital.setBackgroundResource(R.drawable.selectednot_tab_bg)
        }


    }

    override fun getItemCount(): Int {
        return list.size
    }

}