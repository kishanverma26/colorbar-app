package com.android.colorbarapp.Adapter.Managment

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.colorbarapp.Model.Region
import com.android.colorbarapp.R
import com.s.strokeclock.Utils.Common
import com.s.strokeclock.Utils.Common.Companion.getFormatedNumber
import com.s.strokeclock.Utils.Common.Companion.loggg
import kotlinx.android.synthetic.main.regin_wise_item.view.*


class Regin_Wise_Adapter(val context: Context?, val  list: ArrayList<Region>) : RecyclerView.Adapter<Regin_Wise_Adapter.ViewHolder>() {



    class ViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView) {

        val tv_tradename=itemView.tv_tradename
        val tv_netsales=itemView.tv_netsales
        val tv_mtd=itemView.tv_mtd
        val tv_groth=itemView.tv_groth
         val tv_lymtd=itemView.tv_lymtd
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.regin_wise_item, parent, false)
        return ViewHolder(
            view
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener { v ->
//            context!!.startActivity(Intent(context,Order_Detail_Activity::class.java))
        }

        holder.tv_tradename.text = list.get(position).trdename
        holder.tv_netsales.text = Common.roundof(
            Common.get_valuinlaks(list.get(position).netsales)!!).toString()
                    holder . tv_mtd . text = Common.roundof(
                        Common.get_valuinlaks(list.get(position).mtd)!!).toString()

                                holder . tv_lymtd . text = Common.roundof(
                                    Common.get_valuinlaks(list.get(position).lymtd)!!).toString()
                                            loggg ("checkin", list.get(position).growth + ""
                                )
        try {


//        if (list.get(position).growth.toDouble()>0){
        holder.tv_groth.text = list.get(position).growth
//    }else{
//            holder.tv_groth.text = "Nil"
//        }
        }catch ( exe :NumberFormatException){
            loggg("error","exe",exe)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

}