package com.android.colorbarapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.android.colorbarapp.Fragment.Managment.*
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main_managment.*

class MainActivity_Managment : AppCompatActivity() , BottomNavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_managment)

        bottom_navigation.setOnNavigationItemSelectedListener(this)
        supportFragmentManager.beginTransaction().replace(R.id.framlay,
            Trade_Wise_Fragment()
        ).commit()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
//            R.id.nav_home->{
//                supportFragmentManager.beginTransaction().replace(R.id.framlay,
//                    Total_Sales_Fragment()
//                ).commit()
//            }
      R.id.nav_trade->{
                supportFragmentManager.beginTransaction().replace(R.id.framlay,
                    Trade_Wise_Fragment()
                ).commit()
            }

   R.id.nav_group->{
                supportFragmentManager.beginTransaction().replace(R.id.framlay,
                    Category_Wise_Fragment()
                ).commit()
            }

   R.id.nav_state->{
                supportFragmentManager.beginTransaction().replace(R.id.framlay,
                    Region_Wise_Fragment()
                ).commit()
            }

//   R.id.nav_store->{
//                supportFragmentManager.beginTransaction().replace(R.id.framlay,
//                    Store_Wise_Fragment()
//                ).commit()
//            }


        }

        return true
    }

}
