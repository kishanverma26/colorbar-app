package com.android.colorbarapp.Splash;

import android.os.CountDownTimer;

import com.s.strokeclock.Splash.ITimeCount;

public class TimeCount extends CountDownTimer  {

     ITimeCount iTimeCount;



    public  ITimeCount getiTimeCount() {
        return this.iTimeCount;
    }

    public  void setiTimeCount( ITimeCount iTimeCount) {

        this.iTimeCount = iTimeCount;
    }

    public void onTick(long millisUntilFinished) {
        this.getiTimeCount().OnTickListener(millisUntilFinished);
    }

    public void onFinish() {
        this.getiTimeCount().OnFinish();
    }


    public final ITimeCount getITimeCount$production_sources_for_module_app() {
        return this.iTimeCount;
    }

    public final void setITimeCount$production_sources_for_module_app( ITimeCount var1) {

        this.iTimeCount = var1;
    }

    public TimeCount(long millisInFuture, long countDownInterval, ITimeCount iTimeCount) {

        super(millisInFuture, countDownInterval);
        this.iTimeCount = iTimeCount;
    }
}
