package com.s.strokeclock.Splash

interface ITimeCount {

     fun OnTickListener(timetick: Long)
     fun OnFinish()
}