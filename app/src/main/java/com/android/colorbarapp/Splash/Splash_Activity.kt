package com.s.strokeclock.Splash

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.android.colorbarapp.Login_Activity
import com.android.colorbarapp.MainActivity_EBO
import com.android.colorbarapp.MainActivity_Managment
import com.android.colorbarapp.R
import com.android.colorbarapp.Splash.TimeCount
import com.android.colorbarapp.Utils.UserSharedPrefrences

class Splash_Activity : AppCompatActivity() , ITimeCount  {

    private var timeCount: TimeCount? = null
    lateinit var pref: UserSharedPrefrences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_activity)


        this.timeCount = TimeCount(3000L, 1000L, this as ITimeCount)
        pref= UserSharedPrefrences.getInstance(this)


    }


    override fun onResume() {
        super.onResume()
        if (this.timeCount != null) {
            val var10000 = this.timeCount
            if (this.timeCount == null) {

            }

            var10000!!.start()
        }

    }


    override fun onPause() {
        super.onPause()
        if (this.timeCount != null) {
            val var10000 = this.timeCount
            if (this.timeCount == null) {

            }

            var10000!!.cancel()
            //            video.pause();
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        if (this.timeCount != null) {
            val var10000 = this.timeCount
            if (this.timeCount == null) {

            }

            var10000!!.cancel()
        }

    }

    override fun OnTickListener(timetick: Long) {

    }

    override fun OnFinish() {

        if (pref.getlogin()!!){
            if (pref.getrole().equals("zonal_team")){
                finish()
                startActivity(Intent(this, MainActivity_EBO::class.java))
            }else {
                finish()
                startActivity(Intent(this, MainActivity_Managment::class.java))
            }
        }else{
            finish()
            startActivity(Intent(this , Login_Activity::class.java))
        }

    }
}