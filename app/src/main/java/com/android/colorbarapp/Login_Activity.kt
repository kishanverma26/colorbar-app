package com.android.colorbarapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.android.colorbarapp.CustomClasses.DelayedProgressDialog
import com.android.colorbarapp.CustomClasses.MySingleton
import com.android.colorbarapp.Utils.ParamKeys
import com.android.colorbarapp.Utils.UserSharedPrefrences
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.error.AuthFailureError
import com.android.volley.request.StringRequest
import com.s.strokeclock.Utils.Common
import com.s.strokeclock.Utils.Common.Companion.hasText
import com.s.strokeclock.Utils.Common.Companion.loggg
import kotlinx.android.synthetic.main.login_activity.*
import org.json.JSONArray
import org.json.JSONObject

class Login_Activity : AppCompatActivity() ,View.OnClickListener , ParamKeys{


    lateinit var pref: UserSharedPrefrences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity)

        pref= UserSharedPrefrences.getInstance(this)

        bt_login.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v!!.id){

            R.id.bt_login->{
                validation()
            }

        }

    }

    private fun validation() {
        if (hasText(this,et_username,et_username.hint.toString())
                &&hasText(this,et_password,et_password.hint.toString())
                ){
            callAPI(et_username)
        }
    }

    private fun callAPI(view: View) {
        var obj= JSONObject()
        obj.put("username","${et_username.text.toString()}")
        obj.put("password","${et_password.text.toString()}")
        val pd= DelayedProgressDialog()
        pd.show(supportFragmentManager!!,"")
        val stringRequest: StringRequest = object: StringRequest(Method.POST,baseurl_login+"login2", Response.Listener {
            loggg("response login",it)

            var obj= JSONObject(it)

            if(obj.getString("status")=="true")
            {
                pref.settoken(obj.getString("token"))
                pref.setname(obj.getString("name"))

                pref.setjsonrole(obj.getJSONArray("roles").toString())

                pref.setlogin(true)
                if (pref.getjsonrole().length>0){
                    var jary = JSONArray(pref.getjsonrole())
                    for (i in 0..jary.length() - 1) {
                        pref.setrole(jary.getString(0))
                        break
                    }

                }


                if (pref.getrole().equals("zonal_team")){
                    finish()
                    startActivity(Intent(this,MainActivity_EBO::class.java))
                }else {
                    finish()
                    startActivity(Intent(this,MainActivity_Managment::class.java))
                }
            }
            else
            {
                Common.Snakbar("Please enter correct username and password",view)
            }
            pd.cancel()
        },
                Response.ErrorListener {
                    pd.cancel()
                    Common.Snakbar(Common.volleyerror(it),view)
                })
        {

            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray? {
                val str = obj.toString()
                return str.toByteArray()
            }

            override fun getBodyContentType(): String? {
                return "application/json; charset=utf-8"
            }

        }
        val socketTimeout = 50000
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = policy
        MySingleton.getInstance(this).addToRequestQueue(stringRequest)
    }


}
