package com.android.colorbarapp

import android.app.Application
import io.paperdb.Paper

class Application : Application() {


    override fun onCreate() {
        super.onCreate()
        Paper.init(this);

    }
}